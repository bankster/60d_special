<?php $this->load->view('header'); ?>
<!-- Facebook Pixel Code -->
<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','//connect.facebook.net/en_US/fbevents.js');
	
	fbq('init', '632036650272914');
	fbq('track', "PageView");
	fbq('track', 'Lead');
</script>
<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=632036650272914&ev=PageView&noscript=1"
	/></noscript>
<!-- End Facebook Pixel Code -->
</head>
<body id="completeRegister">
	<section class="body-error error-outside">
		<!-- start: page -->
		<div class="row" style="margin-top:100px;">
			<div class="col-lg-12">
				<section class="panel form-wizard" id="w1">
					<!-- <header class="panel-heading">
						<div class="panel-actions">
						</div>
						<h2 class="panel-title"></h2>
					</header> -->
					<div class="panel-body">
						<div class="form-group">
							<h2><center>คำสั่งซื้อเสร็จสมบูรณ์</center></h2>
							<h4><center>ทาง PlanforFIT ได้รับข้อมูลแล้ว ขอบพระคุณครับ</center></h4>
							<div class="contentbox">
								<p>1. สำหรับท่านที่ชำระเงินผ่านบัตรเครดิต สามารถลงทะเบียนได้ทันทีโดย <a href="http://bit.ly/212aKM0 ">คลิกที่นี่</a></p>
								<p>2. สำหรับท่านที่ชำระเงินโดยการโอนเงินผ่านบัญชีธนาคาร สามารถโอนได้ทันที โดยโอนที่<br>
									<span style="margin-top: 5px; display:block;">
										ธนาคารไทยพานิชย์ เลขที่บัญชี 2782211628 <br>
										บริษัท แพลนฟอร์ฟิต จำกัด <br>
										ออมทรัพย์ สาขาเอสพละนาร์ด <br>
										และแจ้งการโอนเงิน <a href="http://bit.ly/212aKM0">คลิกที่นี่</a>
									</span>
								</p>
								<p>3. การสอบถามข้อมูลเพิ่มเติม<br>
								- โทร 093-883-2339<br>
								- inbox fanpage : <a href="fb.com/messages/planforfit/">fb.com/messages/planforfit/</a></p>
							</div>
						</div>
					</div>
					<!-- <div class="panel-footer">
					</div> -->
				</section>
			</div>
		</div>
	</section>
<?php $this->load->view('footer'); ?>