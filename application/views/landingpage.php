<?php $this->load->view('header'); ?>
<!-- Facebook Pixel Code -->
<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','//connect.facebook.net/en_US/fbevents.js');

	fbq('init', '632036650272914');
	fbq('track', "PageView");
</script>
<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=632036650272914&ev=PageView&noscript=1"
	/></noscript>
<!-- End Facebook Pixel Code -->

<!-- analytic -->
<script type="text/javascript">
  !function(){var analytics=window.analytics=window.analytics||[];if(!analytics.initialize)if(analytics.invoked)window.console&&console.error&&console.error("Segment snippet included twice.");else{analytics.invoked=!0;analytics.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","reset","group","track","ready","alias","debug","page","once","off","on"];analytics.factory=function(t){return function(){var e=Array.prototype.slice.call(arguments);e.unshift(t);analytics.push(e);return analytics}};for(var t=0;t<analytics.methods.length;t++){var e=analytics.methods[t];analytics[e]=analytics.factory(e)}analytics.load=function(t){var e=document.createElement("script");e.type="text/javascript";e.async=!0;e.src=("https:"===document.location.protocol?"https://":"http://")+"cdn.segment.com/analytics.js/v1/"+t+"/analytics.min.js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(e,n)};analytics.SNIPPET_VERSION="4.0.0";
  analytics.load("DlN2qNch6ZXOIoSd176xx08Mxkl8WqKS");
  analytics.page();
  }}();
</script>
<!-- end analytic -->

</head>
<body id="paymentinfor">
	<?php $invid = str_replace(".","",microtime(true));?>
	<section class="body-error error-outside">
		<!-- start: page -->
		<div class="row" style="margin-top:100px;">
			<span style="display:none">
			<input type="file" id="files" id="garmin-pic" />
			</span>
			<div class="col-lg-12">
				<section class="panel form-wizard" id="w1">
					<header class="panel-heading">
						<!--div class="panel-actions">
						</div-->
						<h2 class="panel-title">สินค้าบริการ</h2>
					</header>
					<div class="panel-body panel-body-nopadding">
						<div class="wizard-tabs">
							<ul class="wizard-steps">
								<li class="active">
									<a href="#w1-account" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">1</span>
									เลือกชนิดสินค้า
									</a>
								</li>
								<li>
									<a href="#w1-other" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">2</span>
									เพิ่มเติม
									</a>
								</li>
								<li>
									<a href="#w1-profile" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">3</span>
									ข้อมูลส่วนตัว
									</a>
								</li>
								<li>
									<a href="#w1-confirm" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">4</span>
									ยืนยันข้อมูล
									</a>
								</li>
							</ul>
						</div>
						<form id="pff-form" class="form-horizontal" novalidate="novalidate" method="post" action="<?php echo site_url()."/order/new_order";?>">
							<div class="tab-content">
								<div id="w1-account" class="tab-pane active">
									<div class="form-group">
										<label class="col-sm-4 control-label" for="w1-username">สินค้าบริการ</label>
										<div class="col-sm-8">
											<select class="form-control mb-md" id="pff-package" name="pff-package" required>
												<option value="" >กรุณาเลือกสินค้า</option>
												<option value="1">Starter package ราคา 3350</option>
											<!--<option value="2">Standard package ราคา 4290</option>
												<option value="3">Premium package ราคา 9450</option> -->
											</select>
											<!-- <label class="error" for="pff-package"></label> -->
										</div>
									</div>
									<span id="pff-discount" style="display:none"></span>
									<input type="hidden" name="pff-inv" id="pff-inv" value="<?php echo $invid;?>">
									<input type="hidden" name="pff-pic1" id="pff-pic1" value="">
									<input type="hidden" name="pff-pic2" id="pff-pic2" value="">
									<input type="hidden" name="pff-pic3" id="pff-pic3" value="">
									<input type="hidden" name="pff-pic4" id="pff-pic4" value="">
<!--
									<div class="form-group">
										<label class="col-sm-4 control-label" for="pff-invitemail">กรอกอีเมล์รับส่วนลด</label>
										<div class="col-sm-8" >
											<input type="text" value="" placeholder="กรอกอีเมล์เพื่อรับส่วนลด เช่น  email@email.com" class="form-control input-sm " name="pff-getresemail" id="pff-getresemail">
										</div>
									</div>-->


									<div class="form-group">
										<label class="col-sm-4 control-label" for="w1-alliance">โปรโมชั่น</label>
										<div class="col-sm-8">
											<select class="form-control mb-md" id="w1-alliance" name="pff-alliance" required>
												<option value="0">กรุณาเลือกโปรโมชั่น</option>
												<!-- <option value="1">โปรโมชั่น Garmin</option> 
												<option value="2">โปรโมชั่น S-pure</option>-->
												<!-- <option value="2">โปรโมชั่น  Reebok</option> -->
												<!-- <option value="3">สมัครแพ็คคู่</option> -->
												<!--<option value="4">Promo Code/เพื่อนแนะนำ</option>
												 <option value="5">หนังสือ how to ฟิตพิชิตหุ่นนายแบบ</option> -->
												<!-- <option value="6">ลูกค้า Dtac</option> 
												<option value="7">โปรโมชั่น Tofusan</option>
												<option value="8">โปรโมชั่น ทำบุญหุ่นเฟิร์ม: บริจาคเงิน</option>
												<option value="9">โปรโมชั่น ทำบุญหุ่นเฟิร์ม: บริจาคโลหิต, ร่างกาย</option>-->
												<option value="10">สมัครคนเดียว ราคาพิเศษ 3015</option>
												<option value="12">สมัครเป็นคู่ ท่านที่ 2 ลด 50% (เฉลี่ยท่านละ 2513)</option>
												<option value="11">มา 3 จ่าย 2 (เฉลี่ยท่านละ 2234) </option>
											</select>
											<!-- <label class="error" for="pff-alliance"></label> -->
										</div>
									</div>

									<div class="form-group" id="alertRoleCome2pay150per" style="display:none">
										<div class="col-sm-12">
											<div class="alert alert-warning">
												สำหรับโปรโมชั่น สมัครเป็นคู่ ท่านที่ 2 ลด 50% สามารถเลือกโอนเงินได้ 2 แบบ <a href="http://www.planforfit.com/60days/promotion-halfprice.html" target="_blank">คลิกที่นี่เพื่ออ่านรายละเอียด</a>
											</div>
										</div>
									</div>
									<div class="form-group" id="alertRoleCome3pay2" style="display:none">
										<div class="col-sm-12">
											<div class="alert alert-warning">
												สำหรับโปรโมชั่น มา 3 จ่าย 2 สามารถเลือกโอนเงินได้ 3 แบบ <a href="http://www.planforfit.com/60days/promotion-buy3pay2.html" target="_blank">คลิกที่นี่เพื่ออ่านรายละเอียด</a>
											</div>
										</div>
									</div>

									<div class="form-group" id="garmin" style="display:none">
										<label class="col-sm-4 control-label" for="w1-garmin">เคยสั่งซื้อสินค้าเมื่อไร</label>
										<div class="col-sm-8" >
											<select class="form-control mb-md"  name="pff-garmin-buydate" id="pff-garmin-buydate">
												<option value="1">ซื้อก่อน 4 ส.ค. 2559</option>
												<option value="2">ซื้อระหว่าง 4-19 ส.ค 2559</option>
											</select>
											<!--
												<label class="col-sm-4 control-label" for="image1"><span id="garmindetail">กรุณาอัพโหลดภาพนาฬิกาของคุณขณะสวมใส่</span></label>
												<img id="image1" width="200" />
												<input type="file" name="file1" id="file1" required><br>
												<input type="button" value="คลิ๊กเพื่ออัพโหลด" onclick="uploadFile()">
												<progress id="progressBar" value="0" max="100" style="width:300px;"></progress>
												<h3 id="status"></h3>
												<p id="loaded_n_total"></p>-->
										</div>
										<label class="col-sm-4 control-label" for="file1"><span id="garmindetail">กรุณาอัพโหลดภาพนาฬิกาของคุณขณะสวมใส่</span></label>
										<div class="col-sm-8" >
											<img id="image1" width="200" />
											<input type="file" name="file1" id="file1" required><br>
											<input type="button" value="คลิ๊กเพื่ออัพโหลด" onclick="uploadFile()">
											<progress id="progressBar" value="0" max="100" style="width:300px;"></progress>
											<h3 id="status"></h3>
											<p id="loaded_n_total"></p>
										</div>
									</div>
									<div class="form-group" id="howtobook" style="display:none">
										<label class="col-sm-4 control-label" for="file4"><span id="how2detail">กรุณาอัพโหลดภาพใบเสร็จ</span></label>
										<div class="col-sm-8" >
											<img id="image4" width="200" />
											<input type="file" name="file4" id="file4" required><br>
											<input type="button" value="คลิ๊กเพื่ออัพโหลด" onclick="uploadFile4()">
											<progress id="progressBar4" value="0" max="100" style="width:300px;"></progress>
											<h3 id="status4"></h3>
											<p id="loaded_n_total4"></p>
										</div>
									</div>
									<span id="spure" style="display:none">
										<div class="form-group">
											<label class="col-sm-4 control-label" for="pff-alliance-spure-price1">รวมยอดใบเสร็จทั้งหมด</label>
											<div class="col-sm-8" >
												<input type="text" value="0" class="form-control input-sm spurepri" name="pff-alliance-spure-price1" id="pff-alliance-spure-price1" required>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label" for="file2">ภาพสินค้า ใบเสร็จใบที่  1</label>
											<div class="col-sm-8" >
												<img id="image2" width="200" />
												<input type="file" name="file2" id="file2" required><br>
												<input type="button" value="คลิ๊กเพื่ออัพโหลด" onclick="uploadFile2()">
												<progress id="progressBar2" value="0" max="100" style="width:300px;"></progress>
												<h3 id="status2"></h3>
												<p id="loaded_n_total2"></p>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label" for="">ภาพสินค้าใบเสร็จ ใบที่ 2 (ถ้ามี) </label>
											<div class="col-sm-8" >
												<img id="image3" width="200" />
												<input type="file" name="file3" id="file3"><br>
												<input type="button" value="คลิ๊กเพื่ออัพโหลด" onclick="uploadFile3()">
												<progress id="progressBar3" value="0" max="100" style="width:300px;"></progress>
												<h3 id="status3"></h3>
												<p id="loaded_n_total3"></p>
											</div>
										</div>
										<div class="form-group">
											<span class="col-sm-4"></span>
											<span class="col-sm-8"> ใช้เป็นส่วนลดได้ตามยอดจริงที่ระบุในช่อง "รวมยอดใบเสร็จทั้งหมด" แต่ไม่เกิน 300 บาท</span>
										</div>
									</span>
									<span id="friinvit" style="display:none">
										<div class="form-group">
											<label class="col-sm-4 control-label" for="pff-invitemail">กรอกอีเมล์เพื่อนของคุณ</label>
											<div class="col-sm-8" >
												<input type="text" value="" placeholder="กรอกอีเมล์เพื่อนคุณ เช่น  email@email.com" class="form-control input-sm " name="pff-invitemail" id="pff-invitemail" required>
											</div>
										</div>
									</span>
									<span id="Dtac" style="display:none">
										<div class="form-group">
											<label class="col-sm-4 control-label" for="pff-Dtac">กรอกรหัสจาก SMS</label>
											<div class="col-sm-8" >
												<input type="text" value="" placeholder="กด *140*xxx#โทรออก เพื่อรับ code " class="form-control input-sm " name="pff-Dtac" id="pff-Dtac" required>
											</div>
										</div>
									</span>
									<div class="form-group">
										<label class="col-sm-4 control-label">ราคาปกติ</label>
										<div class="col-sm-8">
											<label><span id="pff-price-txt">0</span></label>
											<input type="hidden" name="pff-price" id="pff-price-val" value="">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label">ส่วนลด </label>
										<div class="col-sm-8">
											<label><span id="pff-discount-txt">0</span></label>
											<input type="hidden" name="pff-discount" id="pff-discount-val" value="0">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label"><strong>ราคาสุทธิ</strong> </label>
										<div class="col-sm-8">
											<label><span id="totalPrice">0</span></label>
										</div>
									</div>
								</div>

								<div id="w1-other" class="tab-pane">

									<!-- Modal Center -->
									<a class="modal-basic" id="nooptionmodel" href="#modalCenter"></a>
									<div id="modalCenter" class="modal-block mfp-hide">
										<section class="panel">
											<div class="panel-body">
												<div class="modal-wrapper">
													<div class="modal-text text-center">
														<p>ตาชั่งและคาลิปเปอร์ เป็นอุปกรณ์จำเป็นที่ต้องใช้ตลอดโปรแกรม แนะนำให้ท่านสั่งซื้อในขั้นตอนนี้<br>แต่หากท่านไม่ต้องการสั่งซื้อตอนนี้ สามารถสั่งซื้อได้ในภายหลัง</p>
														<button class="btn btn-default modal-dismiss">กลับไปซื้อ</button>
														<button class="btn btn-primary modal-confirm" id="btncoption">รับทราบ</button>
													</div>
												</div>
											</div>
										</section>
									</div>
									

									<div class="form-group">
										<div class="col-sm-3"></div>
										<div class="col-sm-9">
											<div class="alert alert-warning" id="buyInfo">
												*** การสั่งซื้ออุปกรณ์เสริมสำหรับโปรโมชั่น สมัครเป็นคู่ ท่านที่ 2 ลด 50% จะสั่งซื้อผ่านจอภาพนี้ได้เพียง 1 ชุด หากต้องการสั่งซื้อเพิ่ม สามารถสั่งซื้อได้ที่ลิ้งนี้
												<br><a href="https://planforfit.bentoweb.com/th" target="_blank">https://planforfit.bentoweb.com/th</a> 
											</div>
											<div class="alert alert-warning" id="buyInfo2">
												*** การสั่งซื้ออุปกรณ์เสริมสำหรับโปรโมชั่น มา 3 จ่าย 2 จะสั่งซื้อผ่านจอภาพนี้ได้เพียง 1 ชุด หากต้องการสั่งซื้อเพิ่ม สามารถสั่งซื้อได้ที่ลิ้งนี้
												<br><a href="https://planforfit.bentoweb.com/th" target="_blank">https://planforfit.bentoweb.com/th</a> 
											</div>
										</div>
									</div>

									<div class="form-group">
										<div class="col-sm-4">
											<div class="checkbox-custom" style="padding:0;float:right;">
												<input type="checkbox" name="pff-scales" value="1" id="w1-other1" >
												<label for="w1-other1"></label>
											</div>
										</div>
										<div class="col-sm-8">
											<b>ตาชั่งพกพา เพิ่ม 450 บาท</b><br>
											ใช้ชั่งน้ำหนักอาหาร เพื่อคำนวณสารอาหารตลอดโปรแกรม
										</div>
									</div>

									<div class="form-group">
										<div class="col-sm-4">
											<div class="checkbox-custom" style="padding:0;float:right;">
												<input type="checkbox" name="pff-cliper" value="1" id="w1-other2">
												<label for="w1-other2"></label>
											</div>
										</div>
										<div class="col-sm-8">
											<b>คาลิปเปอร์ เพิ่ม 350 บาท</b><br>
											ใช้วัดเปอร์เซนต์ไขมันสำหรับการลงทะเบียนและส่งประเมินผล
										</div>
									</div>


									<div class="form-group">
										<div class="col-sm-4">

											<div class="checkbox-custom" style="padding:0;float:right;">
												<input type="checkbox" name="pff-shirt" value="1" id="w1-other3" >
												<label for="w1-other3"></label>
											</div>

										</div>

										<!-- Modal Center -->
										<div id="modalCenter2" class="modal-block mfp-hide">
											<section class="panel">
												<div class="panel-body text-center">
													<button class="btn btn-primary modal-dismiss">ปิด</button>
													<div class="modal-wrapper">
														<div class="modal-text text-center">
															<img width="100%" src="<?php echo base_url("assets/imgs/shirt_size.jpg"); ?>" />
														</div>
													</div>
												</div>
											</section>
										</div>


										<div class="col-sm-8">
											<b>เสื้อ 60 Days Challenge เพิ่ม 290 บาท <a class="modal-basic" id="nooptionmodel" href="#modalCenter2">คลิกดูขนาดและความยาวเสื้อ</a> </b><br>
											<select style="display:none" class="form-control mb-md" id="w1-shirtsize" name="pff-shirtsize" id="pff_shirtsize" required>
												<option value="">กรุณาเลือก</option>
												<option value="XS">XS</option>
												<option value="S">S</option>
												<option value="M">M</option>
												<option value="L">L</option>
												<option value="XL">XL</option>
												<option value="2XL">2XL</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-3">
										</div>
										<div class="col-sm-9">
											<span id="optional"></span>
											<b style="font-size:20px;" id="optionaltotal"></b><br>
										</div>
									</div>


								</div>



								<div id="w1-profile" class="tab-pane">

									<div class="form-group">
										<label class="col-sm-4 control-label" for="w1-first-name">ชื่อ</label>
										<div class="col-sm-8">
											<input type="text" class="form-control input-sm" name="pff-first-name" id="pff_name" required>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label" for="w1-last-name">นามสกุล</label>
										<div class="col-sm-8">
											<input type="text" class="form-control input-sm" name="pff-last-name" id="pff_lastname" required>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label" for="w1-email">Email</label>
										<div class="col-sm-8">
											<input type="email" class="form-control input-sm" name="pff-email" id="pff_email" required>
										</div>
									</div>

									<div class="form-group">
										<label class="col-sm-4 control-label" for="w1-address">ที่อยู่</label>
										<div class="col-sm-8">
											<textarea rows="4" cols="50" name="pff-address" id="pff_address" class="form-control" required></textarea>
										</div>
									</div>

								<!-- โปรโมชั่น แนะนำเพื่อน -->
								<div class="alert alert-danger" id="referFriendEmail">
									<h5 style="font-size: 15px" class="text-center">พิเศษ! เพียงกรอก Email แนะนำเพื่อน 2 ท่าน รับส่วนลดเพิ่มทันที</h5>
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group referFriendEmailOne ">
												<label class="col-sm-4 control-label" for="emailRefer1">Email แนะนำเพื่อน 1</label>
												<div class="col-sm-8">
													<input type="email"  name="email-refer-1" id="emailRefer1" class="form-control input-sm" required>
												</div>
											</div>
											<div class="form-group referFriendEmailTwo ">
												<label class="col-sm-4 control-label" for="emailRefer2">Email แนะนำเพื่อน 2</label>
												<div class="col-sm-8">
													<input type="email" name="email-refer-2" id="emailRefer2" class="form-control input-sm"  required>
												</div>
											</div>
										</div>
									</div>
								</div>
									
									
									<div class="form-group">
										<label class="col-sm-4 control-label" for="w1-tel">เบอร์โทรติดต่อ</label>
										<div class="col-sm-8">
											<input type="text" class="form-control input-sm" name="pff-tel" id="pff_tel" required>
										</div>
									</div>
									
									<!-- โปรโมชั่น มา 3 จ่าย 2 -->
								    <div class="form-group mailc3p2 ">
										<label class="col-sm-4 control-label" for="w1-mailc3p2-1">Email 2 <br>(โปรโมชั่น มา 3 จ่าย 2)</label>
										<div class="col-sm-8">
											<input type="email"  name="pff-mailc3p2-1" id="pff_mailc3p2-1" class="form-control input-sm" required>
										</div>
									</div>
									<div class="form-group mailc3p2 ">
										<label class="col-sm-4 control-label" for="w1-mailc3p2-2">Email 3 <br>(โปรโมชั่น มา 3 จ่าย 2)</label>
										<div class="col-sm-8">
											<input type="email" name="pff-mailc3p2-2" id="pff_mailc3p2-2" class="form-control input-sm"  required>
										</div>
									</div>
									
									<!-- โปรท่านที่ 2 ลด 50%-->
									<div class="form-group mail-half-price">
										<label class="col-sm-4 control-label" for="pff_mailHalfPrice">Email 2</label>
										<div class="col-sm-8">
											<input type="email"  name="pff-mail-half-price" id="pff_mailHalfPrice" class="form-control input-sm" required>
										</div>
									</div>	
									
									<div class="form-group">
										<label class="col-sm-4 control-label" for="w1-payment">ช่องทางการชำระเงิน</label>
										<div class="col-sm-8">
											<select class="form-control mb-md" id="w1-payment" name="pff-payment" id="pff_payment">
												<option value="1">โอนเงินผ่านธนาคาร</option>
												<!--	<option value="2">ชำระเงินผ่าน paypal</option> -->
												<option value="3">ชำระเงินผ่านบัตรเครดิต</option>
											</select>
											<div>
												<p class="text-danger">
													*** ขอแนะนำให้โอนเงินแบบทันที เนื่องจากทีมงานจะสามารถตรวจสอบยอดเงินจากการแจ้งโอนเงินได้ง่ายและรวดเร็วขึ้น
												</p>
												<p class="text-danger">ไม่แนะนำให้ตั้งโอนเงินล่วงหน้า เนื่องจากจะทำให้ขั้นตอนการตรวจสอบยอดเงินล่าช้ากว่าปกติ)
												</p>
												<p class="text-danger">
													
												</p>
											</div>
											<div class="alert alert-warning">
												<span id="paymenttext">ธนาคารไทยพาณิชย์ 
												<br>เลขที่บัญชี  <strong>2782211628</strong>
												<br>บริษัท แพลนฟอร์ฟิต จำกัด ออมทรัพย์ สาขาเอสพลานาด</span>
											</div>
										</div>
									</div>
								</div>
								<div id="w1-confirm" class="tab-pane">
									<div class="form-group">
										<label class="col-sm-4 control-label" for="w1-email">เลขที่รายการสั่งซื้อ</label>
										<div class="col-sm-8"> <span id="rs-orderid"> <?php echo $invid;?> </span>  </div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label" for="w1-email">รายการสินค้าที่สั่งซื้อ</label>
										<div class="col-sm-8"> <span id="rs-order"> xxxxxx </span>  </div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label" for="w1-email">ราคา</label>
										<div class="col-sm-8"> <span id="rs-price"> xxxxxx </span></div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label" for="w1-email">ส่วนลด</label>
										<div class="col-sm-8"> <span id="rs-discount"> xxxxxx </span></div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label" for="w1-email">อุปกรณ์เสริม</label>
										<div class="col-sm-8"> <span id="rs-optional"> xxxxxx </span></div>
									</div>
									<div class="alert alert-danger">
										<div class="row">
											<label class="col-sm-4 control-label" for="w1-email">ยอดเงินที่ต้องชำระ</label>
											<div class="col-sm-8"> <strong id="rs-grandtotal" style="font-size: 18px"> xxxxxx </strong></div>
										</div>							
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label" for="w1-email">ชื่อ</label>
										<div class="col-sm-8"> <span id="rs-name"> xxxxxx </span></div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label" for="w1-email">นามสกุล</label>
										<div class="col-sm-8"> <span id="rs-lastname"> xxxxxx </span></div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label" for="w1-email">Email</label>
										<div class="col-sm-8"> <span id="rs-email"> xxxxxx </span></div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label" for="w1-email">ที่อยู่</label>
										<div class="col-sm-8"><span id="rs-address"> xxxxxx </span></div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label" for="w1-email">เบอร์โทรติดต่อ</label>
										<div class="col-sm-8"> <span id="rs-tel"> xxxxxx </span></div>
									</div>
									
									<div class="form-group mail-half-price">
										<label class="col-sm-4 control-label" for="w1-mailc3p2-1">Email 2</label>
										<div class="col-sm-8"> <span id="rs-mailHalfPrice"> xxxxxx </span></div>
									</div>

									<div class="form-group mailc3p2">
										<label class="col-sm-4 control-label" for="w1-mailc3p2-1">Email 2</label>
										<div class="col-sm-8"> <span id="rs-mailc3p2-1"> xxxxxx </span></div>
									</div>
									<div class="form-group mailc3p2">
										<label class="col-sm-4 control-label" for="w1-mailc3p2-2">Email 3</label>
										<div class="col-sm-8"> <span id="rs-mailc3p2-2"> xxxxxx </span></div>
									</div>

									<div class="form-group refer-friend-email">
										<label class="col-sm-4 control-label" for="">Email แนะนำเพื่อน 1</label>
										<div class="col-sm-8"> <span id="rsReferFriendEmail1"> xxxxxx </span></div>
									</div>
									<div class="form-group refer-friend-email">
										<label class="col-sm-4 control-label" for="">Email แนะนำเพื่อน 2</label>
										<div class="col-sm-8"> <span id="rsReferFriendEmail2"> xxxxxx </span></div>
									</div>
									

									
									<div class="form-group">
										<label class="col-sm-4 control-label" for="w1-email">ช่องทางการชำระเงิน</label>
										<div class="col-sm-8"> <span id="rs-payment"> xxxxxx </span></div>
									</div>
									<!--
										<div class="form-group">
											<div class="col-sm-4"></div>
											<div class="col-sm-8">
												<div class="checkbox-custom">

													<input type="checkbox" name="60con" id="w1-terms" required>
													<label for="w1-terms"></label>
												</div>
											</div>
										</div>
										-->
									<div class="form-group">
										<div class="col-sm-4"></div>
										<div class="col-sm-8">
											<div class="checkbox-custom">
												<input type="checkbox" name="terms" id="w1-terms" required>
												<label for="w1-terms">ข้าพเจ้ายอมรับ<strong><a href="http://www.planforfit.com/60days/qualification.html" target="_blank">เงื่อนไขและข้อตกลง</a></strong><br />และยืนยันข้อมูลถูกต้อง</label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="panel-footer">
						<ul class="pager">
							<li class="previous disabled">
								<a><i class="fa fa-angle-left"></i> Previous</a>
							</li>
							<li class="finish hidden pull-right">
								<a>Finish</a>
							</li>
							<li class="next">
								<a>Next <i class="fa fa-angle-right"></i></a>
							</li>
						</ul>
					</div>
				</section>
			</div>
		</div>
		<a class="mb-xs mt-xs mr-xs modal-basic btn btn-info" href="#modalInfo" style="display:none">Info</a>
		<div id="modalInfo" class="modal-block modal-block-info mfp-hide">
			<section class="panel">
				<header class="panel-heading">
					<h2 class="panel-title">ออเดอร์เสร็จสมบูรณ์</h2>
				</header>
				<div class="panel-body">
					<div class="modal-wrapper">
						<div class="modal-icon">
							<i class="fa fa-info-circle"></i>
						</div>
						<div class="modal-text">
							<h4>สำคัญ</h4>
							<div id="poptext">
								<p>เพื่อความรวดเร็วในการเช็คยอดเงิน กรุณาชำระโดยมีเศษสตางค์ต่อท้าย เช่น XXXX.59, XXXX.80 บาท </p>
								<p></p>
								<p>หากมีข้อสงสัยใดๆเพิ่มเติม กรุณาติดต่อ</p>
								<p>fb.com/messages/planforfit/</p>
								<p>หรือ  0938832339 </p>
								<p></p>
								<p>กรุณารอสักครู่ระบบจะเปลี่ยนหน้าอัตโนมัติ</p>
							</div>
						</div>
					</div>
				</div>
				<footer class="panel-footer">
					<div class="row">
						<div class="col-md-12 text-right">
							<!--<button class="btn btn-info modal-dismiss">OK</button>-->
						</div>
					</div>
				</footer>
			</section>
		</div>
	</section>
	<?php $this->load->view('footer'); ?>
