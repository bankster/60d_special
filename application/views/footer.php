<footer>

</footer>

</div>
<!-- /.container -->

<!-- JavaScript -->
<!-- Vendor -->
<script src="<?php echo base_url()?>assets/vendor/jquery/jquery.js"></script>
<script src="<?php echo base_url()?>assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
<script src="<?php echo base_url()?>assets/vendor/bootstrap/js/bootstrap.js"></script>
<script src="<?php echo base_url()?>assets/vendor/nanoscroller/nanoscroller.js"></script>
<script src="<?php echo base_url()?>assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url()?>assets/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script src="<?php echo base_url()?>assets/vendor/magnific-popup/magnific-popup.js"></script>
<script src="<?php echo base_url()?>assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>


<!-- Specific Page Vendor -->
<script src="<?php echo base_url()?>assets/vendor/jquery-validation/jquery.validate.js"></script>
<script src="<?php echo base_url()?>assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
<script src="<?php echo base_url()?>assets/vendor/pnotify/pnotify.custom.js"></script>

<script src="<?php echo base_url()?>assets/vendor/jquery-maskedinput/jquery.maskedinput.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="<?php echo base_url()?>assets/javascripts/theme.js"></script>

<!-- Theme Custom -->
<script src="<?php echo base_url()?>assets/javascripts/theme.custom.js"></script>

<!-- Theme Initialization Files -->
<script src="<?php echo base_url()?>assets/javascripts/theme.init.js"></script>





<script>
$(function() {
  
//Check Alliance and Show or Close panel
$("#w1-alliance").change(function(){
var allianceselect = $(this).find('option:selected').val();
closealliance();

  $(".mailc3p2").hide();
  $(".mail-half-price").hide();
  // ซ่อนคำแนะนำการสั่งซื้อของเพิ่ม สำหรับโปรโมชั่น สมัครเป็นคู่ ท่านที่ 2 ลด 50%
  $("#buyInfo").hide();
  $("#buyInfo2").hide();
  $("#alertRoleCome3pay2").hide();
  $("#alertRoleCome2pay150per").hide();
  $("#referFriendEmail, .refer-friend-email").hide();

  if(allianceselect == 1){
    updateprice();
    $("#garmin").show();
    $("#pff-discount").show();
    $("#pff-garmin-buydate").change();
  }
  else if(allianceselect == 2){
    updateprice();
    $("#spure").show();
    $("#pff-discount").show();
	$(".spurepri").val(0);
    $(".spurepri").change();
	$(".spurepri").removeAttr("readonly");
  }else if(allianceselect == 3){
    //Double price - discount
    duoprice();
  }else if(allianceselect == 4){
	$(".spurepri").val(0);
    updateprice();
    $("#friinvit").show();
    updatediscount(0);
	$(".spurepri").change();
  }else if(allianceselect == 5){
    updateprice();
    $("#howtobook").show();
    updatediscount(335);
  }else if(allianceselect == 6){
    updateprice();
    $("#Dtac").show();
    updatediscount(0);
  }
  else if(allianceselect == 7){
    updateprice();
    $("#spure").show();
    $("#pff-discount").show();
	$(".spurepri").val(0);
    $(".spurepri").change();
	$(".spurepri").removeAttr("readonly");
  }else if(allianceselect == 8){
    updateprice();
    $("#spure").show();
    $("#pff-discount").show();
	$(".spurepri").val(0);
	$(".spurepri").change();
	$(".spurepri").removeAttr("readonly");
  }else if(allianceselect == 9){
    updateprice();
    $("#spure").show();
    $("#pff-discount").show();
	$(".spurepri").val(335);
	updatediscount(335);
    $(".spurepri").change();
	$(".spurepri").attr("readonly", true);
  }
  
  else if(allianceselect == 10){
	  $("#referFriendEmail").show();
    updateprice();
	  updatediscount(335);
  }
  else if(allianceselect == 11){
	  $(".mailc3p2 ").show();
    $("#buyInfo2").show();
    $("#alertRoleCome3pay2").show();
    updateprice();
	  updatediscount(3350);
  }
  else if(allianceselect == 12){
    //$(".mailc3p2 ").show();
    $(".mail-half-price").show();
    $("#buyInfo").show();
    $("#alertRoleCome2pay150per").show();
    updateprice();
    updatediscount(1675);
  }
  else{
    updateprice();
    updatediscount(0);
	$(".spurepri").change();
  }

});

$("#pff-garmin-buydate").change(function(){
var garminselect = $(this).find('option:selected').val();

  if(garminselect == 1){
    $("#garmindetail").text("กรุณาอัพโหลดภาพนาฬิกาของคุณขณะสวมใส่");
  }
  else{
    $("#garmindetail").text("กรุณาอัพโหลดใบเสร็จ");
  }

});


function duoprice(){
  var pffpackage = $("#pff-package").find('option:selected').val();
  var pricetxt = $("#pff-price-txt");
  var priceval = $("#pff-price-val");

  if(pffpackage==1){pricetxt.text("6700");priceval.val("5026");updatediscount(1674);}
  else if(pffpackage==2){pricetxt.text("8580");priceval.val("6980");updatediscount(800);}
  else if(pffpackage==3){pricetxt.text("18900");priceval.val("16900");updatediscount(1000);}
  else{pricetxt.text("0");priceval.val("0");updatediscount(0);}

}



function closealliance(){
  $("#garmin").hide();
  $("#spure").hide();
  $("#pff-discount").hide();
  $("#friinvit").hide();
  $("#howtobook").hide();
  $("#Dtac").hide();
}


// Discount calculate fucntion
$(".spurepri").change(function(){
  var discount = 0;

   // 50% Discount
   //discount = parseInt($(this).val())/2;
   discount = parseInt($(this).val());
   //console.log("spidiscount : "+discount);
   if(isNaN(discount) || discount == " "){
     discount = 0 ;
   }

    if(discount >= 335){
      discount = 335 ;
      updatediscount(discount);
    }else{
      updatediscount(discount);
    }

    getresemailajax();

});

$("#pff-garmin-buydate").change(function(){
  buydate = $("#pff-garmin-buydate");
  if(buydate.val() == 1){
    updatediscount(250);
  }else if(buydate.val() == 2){
    updatediscount(500);
  }else{updatediscount(0);}
});





//Update Discount Price
function updatediscount(price)
{
  var discounttxt = $("#pff-discount-txt");
  var discountval = $("#pff-discount-val");

  discounttxt.text(price);
  discountval.val(price);

  //getresemailajax();
}

function update2discount(price){

  var discounttxt = $("#pff-discount-txt");
  var discountval = $("#pff-discount-val");
  var olddiscount =  parseInt(discountval.val()) ;
  var newdiscount = 0 ;
  newdiscount = olddiscount+price ;


  if((newdiscount) >= 335){
    newdiscount = 335 ;
  }

  discounttxt.text(newdiscount);
  discountval.val(newdiscount);

}


//Update Package Price
$("#pff-package").change(function(){
  updateprice();
});
function updateprice() {
  var pffpackage = $("#pff-package").find('option:selected').val();
  var pricetxt = $("#pff-price-txt");
  var priceval = $("#pff-price-val");
  var totalPrice = $("#totalPrice");

  if(pffpackage==1){
    pricetxt.text("3350");
    priceval.val("3350");

    if (allianceselect === 10) {
      totalPrice.text("3015");
    } else {
      totalPrice.text("3350");
    }

    
  }
  else if(pffpackage==2){
    pricetxt.text("4290");
    priceval.val("4290");
  }
  else if(pffpackage==3){
    pricetxt.text("9450");
    priceval.val("9450");
  }
  else{
    pricetxt.text("0");
    priceval.val("0");
    priceval.val("0");
    updatediscount(0);
  }

  var allianceselect = $("#w1-alliance").find('option:selected').val();
  if(allianceselect == 3){
    //Double price - discount
    duoprice();
  } else if(allianceselect == 10) {
    pricetxt.text("3350");
    priceval.val("3350");
    totalPrice.text("3015");
  } else if(allianceselect == 11) {
	  pricetxt.text("10050");
	  priceval.val("10050");
    totalPrice.text("6700");
  } else if(allianceselect == 12) {
    pricetxt.text("6700");
    priceval.val("6700");
    totalPrice.text("5025");
  }
  updateoptiontotal(0);
}


function getprice(){

  var pffpackage = $("#pff-package").find('option:selected').val();
  var price = 0;

  if(pffpackage==1){
    price = 3350;
  }
  else if(pffpackage==2){
    price = 4290;
  }
  else if(pffpackage==3){
    price = 9450;
  }
  else{
    price = 0;
  }
  
  
  var allianceselect = $("#w1-alliance").find('option:selected').val();
   if(allianceselect == 11){
    price = 10050 ;
   } else if (allianceselect == 12) {
    price = 6700;
   }
   //console.log(price);
  
  
  return price ;
}

  'use strict';

/*
Pop upform
*/
$('.modal-basic').magnificPopup({
  type: 'inline',
  preloader: false,
  modal: true
});	$(document).on('click', '.modal-dismiss', function (e) {
  e.preventDefault();
  $.magnificPopup.close();
});




$("#w1-payment").change(function(){

  var payoption = $(this).find('option:selected').val();

  if(payoption == 1)
  {
    $('#paymenttext').text('ธนาคารไทยพานิชย์ เลขที่บัญชี   2782211628 บริษัท แพลนฟอร์ฟิต จำกัด ออมทรัพย์ สาขาเอสพลานาด');
  }else{
    $('#paymenttext').text('หลังจากยืนยันคำสั่งซื้อในขั้นตอนที่ 3 แล้ว ระบบจะพาท่านไปสู่หน้ากรอกรายละเอียดบัตรเครดิต');
  }

});


//friend invit Checkmail
$("#pff-invitemail").change(function(){

  var emailval = $("#pff-invitemail").val();
   $.ajax({
      url: "<?php echo site_url()."/order/mailcheck"?>",
      type: "POST",
      data: { email:emailval},
      dataType: "html",
      success: function (data) {
        //console.log(data.replace(/[\r\n]/g, ""));
        if(data == 1)
        {
          updatediscount(335);
        }else if(data == 0){
          updatediscount(0);
        }else if(data){
          updatediscount(data);
        }
      }
    }).done(function( html ) {

   });;
});


//friend invit Checkmail
$("#pff-getresemail").change(function(){
  getresemailajax();
});

function getresemailajax(){
  var emailval = $("#pff-getresemail").val();
   $.ajax({
      url: "<?php echo site_url()."/order/mailcheckgetres"?>",
      type: "POST",
      data: { email:emailval},
      dataType: "html",
      success: function (data) {
        //console.log(data.replace(/[\r\n]/g, ""));
        if(data == 1)
        {
          update2discount(150);
        }else{
          update2discount(0);
        }
      }
    }).done(function( html ) {
   });;
}

//Ussd Dtac
$("#pff-Dtac").change(function(){
  var emailval = $("#pff-Dtac").val();
   $.ajax({
      url: "<?php echo site_url()."/order/mailcheck"?>",
      type: "POST",
      data: { email:emailval},
      dataType: "html",
      success: function (data) {
        //console.log(data.replace(/[\r\n]/g, ""));
        if(data == 500)
        {
          updatediscount(500);
        }else if(data == 1000)
        {
          updatediscount(1000);
        }else{
          updatediscount(0);
        }
      }
    }).done(function( html ) {
   });;
});



/*
Form Process
*/
var $w1finish = $('#w1').find('ul.pager li.finish'),
  $w1validator = $("#w1 form").validate({
  highlight: function(element) {
    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
  },
  success: function(element) {
    $(element).closest('.form-group').removeClass('has-error');
    $(element).remove();
  },
  errorPlacement: function( error, element ) {
    element.parent().append( error );
  }
});

$w1finish.on('click', function( ev ) {
  ev.preventDefault();
  var validated = $('#w1 form').valid();

  if ( validated ) {

  $( "#pff-form" ).submit();

  /*
      $(".modal-basic").click();
       $.ajax({
        url: "<?php echo site_url()."/order/new_order"?>",
        type: "POST",
        data: $("#pff-form").serialize(),
        dataType: "html",
        success: function (data) {
          //alert(data.replace(/[\r\n]/g, ""));
          //console.log(data.replace(/[\r\n]/g, ""));
          window.location.href = data.replace(/[\r\n]/g, "");
        }
      }).done(function( html ) {
        //window.location.href = 'http://planforfit.com/60d/index.php/order/ordercomplete';
       });;
    */


  }
});


var confirmoption = false ;


$(document).on('click', '.modal-confirm', function (e) {
  confirmoption = true ;
  $(".next").click();

  e.preventDefault();
  $.magnificPopup.close();
});


var optional1 = false ;
var optional2 = false ;
var optional3 = false ;

//Add Event Check box Tab Tool Option page
$("#w1-other1").click(function(){
  if($(this).is(':checked')){
    $("#optional").append('<span id="w1option1">+ ตาชั่งพกพา 450 บาท<br></span>');
    updateoptiontotal(450);
    optional1 = true ;
  }else{
    $("#w1option1").remove();
    updateoptiontotal(-450);
    optional1 = false ;
  }
});

$("#w1-other2").click(function(){
  if($(this).is(':checked')){
    $("#optional").append('<span id="w1option2">+ คาร์ลิปเปอร์ 350 บาท<br></span>');
    updateoptiontotal(350);
    optional2 = true ;
  }else{
    $("#w1option2").remove();
    updateoptiontotal(-350);
    optional2 = false ;
  }
});

$("#w1-other3").click(function(){
  if($(this).is(':checked')){
    $("#optional").append('<span id="w1option3">+ เสื้อ 60Days Challenge 290 บาท<br></span>');
    updateoptiontotal(290);
    $("#w1-shirtsize").show();
    optional3 = true ;
  }else{
    $("#w1option3").remove();
    updateoptiontotal(-290);
    $("#w1-shirtsize").hide();
    optional3 = false ;
  }
});


//Update Option total text
var optionaltotal = 0 ;
function updateoptiontotal(price){
  optionaltotal += price ;
  total = getprice();
  $("#optionaltotal").text('ยอดสุทธิ '+(parseInt(total)-parseInt($("#pff-discount-txt").text())+optionaltotal)+' บาท' );
}




$('#w1').bootstrapWizard({
  tabClass: 'wizard-steps',
  nextSelector: 'ul.pager li.next',
  previousSelector: 'ul.pager li.previous',
  firstSelector: null,
  lastSelector: null,
  onNext: function( tab, navigation, index, newindex ) {
    var SubPromotionSelected = $("#w1-alliance").find('option:selected').val();

    console.log('tab index:', index);
    console.log('sub promotion:', SubPromotionSelected);

    var validated = $('#w1 form').valid();
    if( !validated ) {
      $w1validator.focusInvalid();
      return false;
    }

    // Check Upload Image
    if(index == navigation.find('li').size() - 3 && $("#w1-alliance").find('option:selected').val() != 0 && $("#w1-alliance").find('option:selected').val() != 4 
	&& $("#w1-alliance").find('option:selected').val() != 10 && $("#w1-alliance").find('option:selected').val() != 11 && $("#w1-alliance").find('option:selected').val() != 12){
      if($("#pff-pic2").val() == ""){
        alert("โปรดคลิ๊กปุ่มอัพโหลดรูปภาพ");
        return false;
      }
    }


    // Update Data in confirm page
    if(index == navigation.find('li').size() - 1){
      comfirmtabdata();
    }


    // Check Confirm popup
    if(index == navigation.find('li').size() - 2){
      //Show popup confirm
      if(optional2 == false || optional1 == false)
      {
          $("#nooptionmodel").click();
      }else{
        confirmoption = true;
      }
      //Check submit non Select
      if(confirmoption){
          return true ;
      }else {
          return false ;
      }
    }

    if(index == navigation.find('li').size() - 3){
      $("#optionaltotal").text('ยอดสุทธิ '+(parseInt(total)-parseInt($("#pff-discount-txt").text())+optionaltotal)+' บาท' );
    }




  },
  onTabClick: function( tab, navigation, index, newindex ) {
    return false;
    /*
    if ( newindex == index + 1 ) {
      return this.onNext( tab, navigation, index, newindex);
    } else if ( newindex > index + 1 ) {
      return false;
    } else {
      return true;
    }*/
  },
  onTabChange: function( tab, navigation, index, newindex ) {

    //Set init param for Check Select Option tab
    if(index == navigation.find('li').size() - 2){
      confirmoption = false ;
      //console.log(confirmoption);
    }

    var totalTabs = navigation.find('li').size() - 1;
    $w1finish[ newindex != totalTabs ? 'addClass' : 'removeClass' ]( 'hidden' );
    $('#w1').find(this.nextSelector)[ newindex == totalTabs ? 'addClass' : 'removeClass' ]( 'hidden' );

  }
});

function comfirmtabdata()
{

  var allianceselect = $("#w1-alliance").find('option:selected').val();
  //console.log(allianceselect);
  var total = getprice() ;

  if(allianceselect == 3){
    total *= 2 ;
  }

  console.log($("#emailRefer1").val());
  console.log($("#emailRefer2").val());

  if (allianceselect == 10
    && $("#emailRefer1").val().length != 0
    && $("#emailRefer2").val().length != 0
  ){
    //console.log("Y");
    var extraDiscount = 1116;
    $("#rs-discount ").text(extraDiscount);
    $("#rs-grandtotal").text((parseInt(total) - extraDiscount) + optionaltotal);
    $("#rsReferFriendEmail1").text($("#emailRefer1").val());
    $("#rsReferFriendEmail2").text($("#emailRefer2").val());
    $(".refer-friend-email").show();
  } else {
    //console.log("N");
    $("#rs-discount	").text($("#pff-discount-txt").text());
    $("#rs-grandtotal").text(parseInt(total)-parseInt($("#pff-discount-txt").text())+optionaltotal);
  }

  $("#rs-orderid").text();
  $("#rs-order").text( $("#pff-package").find('option:selected').text() );
  $("#rs-price").text(total);
  $("#rs-optional").text(optionaltotal);
  $("#rs-name ").text($("#pff_name").val());
  $("#rs-lastname ").text($("#pff_lastname").val());
  $("#rs-email").text($("#pff_email").val());
  $("#rs-address  ").text($("#pff_address").val());
  $("#rs-tel").text($("#pff_tel").val() );
  
  //console.log($("#pff_mailc3p2-1").val());
  $("#rs-mailc3p2-1").text($("#pff_mailc3p2-1").val());
  $("#rs-mailc3p2-2").text($("#pff_mailc3p2-2").val());

  $("#rs-mailHalfPrice").text($("#pff_mailHalfPrice").val());
  
  $("#rs-payment").text( $("#w1-payment").find('option:selected').text() );
}


// Upload Picture

$("#file1").change(function(){
  var reader = new FileReader();
    reader.onload = function (e) {
    // get loaded data and render thumbnail.
     document.getElementById("image1").src = e.target.result;
  };
  reader.readAsDataURL(this.files[0]);
});


$("#file2").change(function(){
  var reader = new FileReader();
    reader.onload = function (e) {
    // get loaded data and render thumbnail.
     document.getElementById("image2").src = e.target.result;
  };
  reader.readAsDataURL(this.files[0]);
});



$("#file3").change(function(){
  var reader = new FileReader();
    reader.onload = function (e) {
    // get loaded data and render thumbnail.
     document.getElementById("image3").src = e.target.result;
  };
  reader.readAsDataURL(this.files[0]);
});


});


function _(el){
  return document.getElementById(el);
}


function uploadFile(){
  var file = _("file1").files[0];
  // alert(file.name+" | "+file.size+" | "+file.type);
  var formdata = new FormData();
  formdata.append("file1", file);
  var ajax = new XMLHttpRequest();
  ajax.upload.addEventListener("progress", progressHandler, false);
  ajax.addEventListener("load", completeHandler, false);
  ajax.addEventListener("error", errorHandler, false);
  ajax.addEventListener("abort", abortHandler, false);
  ajax.open("POST", "<?php echo site_url()?>/order/picupload");
  ajax.send(formdata);
}
function progressHandler(event){
  _("loaded_n_total").innerHTML = "Uploaded "+event.loaded+" bytes of "+event.total;
  var percent = (event.loaded / event.total) * 100;
  _("progressBar").value = Math.round(percent);
  _("status").innerHTML = Math.round(percent)+"% uploaded... please wait";
}
function completeHandler(event){

  _("pff-pic1").value = event.target.responseText.replace(/[\r\n]/g, "");
  _("status").innerHTML = event.target.responseText+"Upload Complete";
  _("progressBar").value = 0;
}
function errorHandler(event){
  _("status").innerHTML = "Upload Failed";
}
function abortHandler(event){
  _("status").innerHTML = "Upload Aborted";
}


function uploadFile2(){

	var fileselected = $("#file2").val();

	if(!fileselected){
		alert("กรุณาคลิกปุ่ม Browse เพื่อเลือกรูปใบเสร็จ");
		return false;
	}


  var file = _("file2").files[0];
  // alert(file.name+" | "+file.size+" | "+file.type);
  var formdata = new FormData();
  formdata.append("file2", file);
  var ajax = new XMLHttpRequest();
  ajax.upload.addEventListener("progress", progressHandler2, false);
  ajax.addEventListener("load", completeHandler2, false);
  ajax.addEventListener("error", errorHandler2, false);
  ajax.addEventListener("abort", abortHandler2, false);
  ajax.open("POST", "<?php echo site_url()?>/order/picupload");
  ajax.send(formdata);
}
function progressHandler2(event){
  _("loaded_n_total2").innerHTML = "Uploaded "+event.loaded+" bytes of "+event.total;
  var percent = (event.loaded / event.total) * 100;
  _("progressBar2").value = Math.round(percent);
  _("status2").innerHTML = Math.round(percent)+"% uploaded... please wait";
}
function completeHandler2(event){
  _("pff-pic2").value = event.target.responseText.replace(/[\r\n]/g, "");
  _("status2").innerHTML = event.target.responseText+"Upload Complete";
  _("progressBar2").value = 0;
}
function errorHandler2(event){
  _("status2").innerHTML = "Upload Failed";
}
function abortHandler2(event){
  _("status2").innerHTML = "Upload Aborted";
}



function uploadFile3(){


	var fileselected = $("#file3").val();
	if(!fileselected){
		alert("กรุณาคลิกปุ่ม Browse เพื่อเลือกรูปใบเสร็จ");
		return false;
	}


  var file = _("file3").files[0];
  // alert(file.name+" | "+file.size+" | "+file.type);
  var formdata = new FormData();
  formdata.append("file3", file);
  var ajax = new XMLHttpRequest();
  ajax.upload.addEventListener("progress", progressHandler3, false);
  ajax.addEventListener("load", completeHandler3, false);
  ajax.addEventListener("error", errorHandler3, false);
  ajax.addEventListener("abort", abortHandler3, false);
  ajax.open("POST", "<?php echo site_url()?>/order/picupload");
  ajax.send(formdata);
}
function progressHandler3(event){
  _("loaded_n_total3").innerHTML = "Uploaded "+event.loaded+" bytes of "+event.total;
  var percent = (event.loaded / event.total) * 100;
  _("progressBar3").value = Math.round(percent);
  _("status3").innerHTML = Math.round(percent)+"% uploaded... please wait";
}
function completeHandler3(event){
  _("pff-pic3").value = event.target.responseText.replace(/[\r\n]/g, "");
  _("status3").innerHTML = event.target.responseText+"Upload Complete";
  _("progressBar3").value = 0;
}
function errorHandler3(event){
  _("status3").innerHTML = "Upload Failed";
}
function abortHandler3(event){
  _("status3").innerHTML = "Upload Aborted";
}



function uploadFile4(){
  var file = _("file4").files[0];
  // alert(file.name+" | "+file.size+" | "+file.type);
  var formdata = new FormData();
  formdata.append("file4", file);
  var ajax = new XMLHttpRequest();
  ajax.upload.addEventListener("progress", progressHandler4, false);
  ajax.addEventListener("load", completeHandler4, false);
  ajax.addEventListener("error", errorHandler4, false);
  ajax.addEventListener("abort", abortHandler4, false);
  ajax.open("POST", "<?php echo site_url()?>/order/picupload");
  ajax.send(formdata);
}
function progressHandler4(event){
  _("loaded_n_total4").innerHTML = "Uploaded "+event.loaded+" bytes of "+event.total;
  var percent = (event.loaded / event.total) * 100;
  _("progressBar4").value = Math.round(percent);
  _("status4").innerHTML = Math.round(percent)+"% uploaded... please wait";
}
function completeHandler4(event){
  _("pff-pic4").value = event.target.responseText.replace(/[\r\n]/g, "");
  _("status4").innerHTML = event.target.responseText+"Upload Complete";
  _("progressBar4").value = 0;
}
function errorHandler4(event){
  _("status4").innerHTML = "Upload Failed";
}
function abortHandler4(event){
  _("status4").innerHTML = "Upload Aborted";
}




/*
Payment inform
*/

//Search Data by INV
$("#searchinv").click(function(){
  var inv = $("#invid").val();
       $.ajax({
        url: "<?php echo site_url()."/order/ordersearch/"?>",
        type: "POST",
        data: { invid: inv},
        dataType: "html",
        success: function (data) {
           var obj = JSON.parse(data);
		   if(obj[0].email == 'not found')
		   {
			   alert('ไม่พบรายการสั่งซื้อ');
		   }else{
			   $("#email").val(obj[0].email);
         $("#name").val(obj[0].firstname+" "+obj[0].lastname);
         $("#tel").val(obj[0].tel);
         $("#address").val(obj[0].address);
		   }
        }
        }).done(function( html ) {
          //alert('');
      });;
})



//Search Data by Email
$("#searchemail").click(function(){/*
  var email = $("#emailcheck").val();
       $.ajax({
        url: "<?php echo site_url()."/order/ordersearchemail/"?>",
        type: "POST",
        data: { email: email},
        dataType: "html",
        success: function (data) {
           var obj = JSON.parse(data);

		   if(obj[0].email == 'not found')
		   {
				 alert('ไม่พบรายการสั่งซื้อ');

		   }else{

				 $("#email").val(obj[0].email);
				 $("#name").val(obj[0].firstname+" "+obj[0].lastname);
				 $("#tel").val(obj[0].tel);
				 $("#address").val(obj[0].address);
				 $("#inv").text(obj[0].invid);
				 $("#invid").val(obj[0].invid);


				 $("#priceh").val(obj[0].price);
         $("#price").text(obj[0].price);
         $("#discounth").val(obj[0].discount);
         $("#discount").text(obj[0].discount);
         $("#digitalscaleh").val(obj[0].digitalscale);
         $("#digitalscale").text(obj[0].digitalscale);
         $("#caliperh").val(obj[0].caliper);
         $("#caliper").text(obj[0].caliper);
         $("#shirth").val(obj[0].shirt);
         $("#shirt").text(obj[0].shirt);
         $("#totalh").val(obj[0].total);
         $("#total").text(obj[0].total);

				 //hide email field after get data
				 //$("#checkform").hide();
				 $("#emailcheck").attr("disabled", "disabled");



				 //Active Other input after get data
				 $("#name").removeAttr('disabled');
				 $("#tel").removeAttr('disabled');
				 $("#address").removeAttr('disabled');
				 $("#btnsubmit").removeAttr('disabled');

				 // Show Shirt for Who buy Package Standard or Premiumn
				 if(obj[0].package == 2 || obj[0].package == 3)
				 {
					 $("#shirtform").show();
				 }


		   }
        }
        }).done(function( html ) {
          //alert('');
      });*/
})




</script>




</body>

</html>
