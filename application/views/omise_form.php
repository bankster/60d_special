<!DOCTYPE html>
<html lang="en" class="fixed">

    <head>
		<!-- Basic -->
		<meta charset="UTF-8">

		<title>planforfit 60day challange</title>
		<meta name="keywords" content="planforfit" />
		<meta name="description" content="planforfit 60day challange">
		<meta name="author" content="okler.net">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/bootstrap/css/bootstrap.css" />

		<link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/pnotify/pnotify.custom.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo base_url()?>assets/stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="<?php echo base_url()?>assets/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo base_url()?>assets/stylesheets/theme-custom.css">

		<!-- Head Libs -->
		<script src="<?php echo base_url()?>assets/vendor/modernizr/modernizr.js"></script>


	 <!-- Omise -->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> 
	 <script src="https://cdn.omise.co/omise.js"></script>
	 <script>
	  Omise.setPublicKey("pkey_53ja07x6shq2y3i7f72");
	</script>
	
	
	
	<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '632036650272914');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=632036650272914&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

</head>
    <body style="background: #484848 url(http://p4fpullzone.planforfit.netdna-cdn.com/wp-content/themes/bucket-child/img/fullad-604.jpg) no-repeat fixed center top;">

	
							<section class="body-error error-outside">
	
							<div class="row" style="margin-top:100px;">

							
							
							<div class="col-lg-2"></div>
							
							<div class="col-lg-8">
								<section class="panel form-wizard" id="w1">
									<header class="panel-heading">
										<div class="panel-actions">
										</div>
						
										<h2 class="panel-title">ชำระเงินผ่านบัตรเครดิต</h2>
									</header>
									
									
									<div class="panel-body">
									
									
	
									   <form action="<?php echo site_url("order/omisesubmit")?>" method="post" id="checkout" class="form-horizontal form-bordered">
							
									  
									   <input type="hidden" name="omise_token">
									   <input type="hidden" name="invid" value="<?php echo $invid; ?>">

  										<div class="form-group">
  											<div clsss="col-sm-12" style="color: red; margin: 0 15px;">
  												*** เมื่อกดปุ่มชำระเงินแล้ว ให้รอสักครู่ จนระบบแสดงข้อความว่า "คำสั่งซื้อเสร็จสมบูรณ์" จึงจะถือว่า จบขั้นตอนการสั่งซื้อ (ไม่ต้องกด "ชำระเงิน" ซ้ำ เนื่องจากจะทำให้ระบบตัดเงินซ้ำอีกครั้ง)
  											</div>
  										</div>

  										<div class="form-group">
												<label class="col-sm-4 control-label" for="name">ชื่อ *</label>
												<div class="col-sm-8">
													<input type="text" data-omise="holder_name" class="form-control input-sm" name="name" id="name" required>
												</div>
										</div>
										
										
										<div class="form-group">
												<label class="col-sm-4 control-label" for="card">เลขบัตร *</label>
												<div class="col-sm-8">
													<input type="text" data-omise="number" placeholder="____-____-____-____" data-input-mask="9999-9999-9999-9999" class="form-control input-sm" name="card" id="card" required>
												</div>
										</div>
										
										
										<div class="form-group">
												<label class="col-sm-4 control-label" for="month">เดือน *</label>
												<div class="col-sm-3">
													<input type="text" data-omise="expiration_month" class="form-control input-sm" name="month" id="month" required>
												</div>
												
												<label class="col-sm-2 control-label" for="year">ปี *</label>
												<div class="col-sm-3">
													<input type="text" data-omise="expiration_year" class="form-control input-sm" name="year" id="year" required>
												</div>
										</div>
										
										
										<div class="form-group">
												<label class="col-sm-4 control-label" for="CCV">CCV *</label>
												<div class="col-sm-8">
													<input type="text" data-omise="security_code" class="form-control input-sm" name="CCV" id="CCV" required>
												</div>
										</div>
										
										
										<div class="form-group">
												<label class="col-sm-4 control-label"></label>
												<div class="col-sm-8">
													<input type="submit" id="create_token" class="mb-xs mt-xs mr-xs btn btn-success" value="ชำระเงิน">
												</div>
										</div>
										
										
										<div class="form-group">
											<div class="col-sm-9"> <div id="token_errors" style="color:red"></div></div>
											<div class="col-sm-3">
											Power by planforfit
											<img width="100%" src="<?php echo base_url()?>assets/imgs/secured_by_omise_v2.png" />
											</div>
										</div>

										  
										</form>

				
									
									
											
								
									</div>
									<div class="panel-footer">

				
									</div>
								</section>
						</div>

						
						<div class="col-lg-2"></div>
	
	
	

										</section>

		<!-- JavaScript -->
		<!-- Vendor -->
		<script src="<?php echo base_url()?>assets/vendor/jquery/jquery.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>

		
		<!-- Specific Page Vendor -->
		<script src="<?php echo base_url()?>assets/vendor/jquery-validation/jquery.validate.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/pnotify/pnotify.custom.js"></script>
		
		<script src="<?php echo base_url()?>assets/vendor/jquery-maskedinput/jquery.maskedinput.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url()?>assets/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url()?>assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url()?>assets/javascripts/theme.init.js"></script>




	<script>
		$("#checkout").submit(function () {

		  var form = $(this);

		  // Disable the submit button to avoid repeated click.
		  form.find("input[type=submit]").prop("disabled", true);

		  // Serialize the form fields into a valid card object.
		  var card = {
			"name": form.find("[data-omise=holder_name]").val(),
			"number": form.find("[data-omise=number]").val(),
			"expiration_month": form.find("[data-omise=expiration_month]").val(),
			"expiration_year": form.find("[data-omise=expiration_year]").val(),
			"security_code": form.find("[data-omise=security_code]").val()
		  };

		  // Send a request to create a token then trigger the callback function once
		  // a response is received from Omise.
		  //
		  // Note that the response could be an error and this needs to be handled within
		  // the callback.
		  Omise.createToken("card", card, function (statusCode, response) {
			if (response.object == "error") {
			  // Display an error message.
			  $("#token_errors").html(response.message);

			  // Re-enable the submit button.
			  form.find("input[type=submit]").prop("disabled", false);
			} else {
			  // Then fill the omise_token.
			  form.find("[name=omise_token]").val(response.id);

			  // Remove card number from form before submiting to server.
			  form.find("[data-omise=number]").val("");
			  form.find("[data-omise=security_code]").val("");

			  // submit token to server.
			  form.get(0).submit();
			};
		  });

		  // Prevent the form from being submitted;
		  return false;

		});
	</script>
</body>
</html>