<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Order extends CI_Controller {


    function __construct() {
        // Call the Model constructor
        parent::__construct();
		$this->load->model('memail');
		$this->load->model('sixtydaypromo');
		$this->load->model('payment');
    }

    public function index() {

    }


	public function mailcheck(){

		$mail = $_POST['email'];

		$query = $this->db->get_where('shop_email_promo', array('email' => $mail));
		if ($query->num_rows() > 0)
		{
			echo 1 ;
		}else{

      $query = $this->db->get_where('shop_ussd_promo', array('ussd_code' => $mail))->result_array();
      if (count($query) > 0)
      {
        echo $query[0]['discount'] ;
      }else{
        echo 0 ;
      }

    }

	}//End Mail Check


  public function mailcheckgetres(){

    $mail = $_POST['email'];

    $query = $this->db->get_where('shop_email_getresponse', array('email' => $mail));
    if ($query->num_rows() > 0)
    {
      echo 1 ;
    }else{
      echo 0 ;
    }

  }//End Mail Check



	//load view ordercomplete
	public function ordercomplete()
	{
		$this->load->view('complete');
	}

	//load view payment inform
	public function paymentinform()
	{
		$this->load->view('paymentinform');
	}

	//Insert paymentinform to database
	public function paymentinformsubmit(){

			$invid = $_POST['invid'];
			//$join60d_reson = $_POST['join60d_reson'];
			$name = 	$_POST['name'];
			//$sex =	$_POST['sex'];
			$tel =	$_POST['tel'];
			//$age =	$_POST['age'];
			$email = $_POST['email'];
			//$height = $_POST['height'];
			//$weight = 	$_POST['weight'];
			$address =	$_POST['address'];
			//$fatpercent =	$_POST['fatpercent'];
			$target =	$_POST['target'];
			$disease = $_POST['disease'];
			//$foodprogram = $_POST['foodprogram'];
			//$exerciseprogram = 	$_POST['exerciseprogram'];
			$fitness =	$_POST['fitness'];
			$balance =	$_POST['balance'];
			$payment =	$_POST['payment'];
			$date =	$_POST['date'];
			$time =	$_POST['time'];
			//$condition =	$_POST['60dcondition'];
			$knowpff =	$_POST['knowpff'];
			$facebookname =	$_POST['facebookname'];
			$facebooklink =	$_POST['facebooklink'];
			$knowpffnew = "";
			$shirt = $this->input->post("shirt");


      $price = $this->input->post("price");
      $discount = $this->input->post("discount");
      $digitalscale = $this->input->post("digitalscale");
      $caliper = $this->input->post("caliper");
      $total = $this->input->post("total");



			foreach($knowpff as $val)
			{
				$knowpffnew .= $val."," ;
			}


			$data = array(
				'invid' => $invid ,
				//'join60d_reson' => $join60d_reson ,
				'name' => $name ,
				//'sex' => $sex ,
				'tel' => $tel ,
				//'age' => $age ,
				'email' => $email ,
				//'height' => $height ,
				//'weight' => $weight ,
				'address' => $address ,
				//'fatpercent' => $fatpercent ,
				'target' => $target ,
				'disease' => $disease ,
				//'foodprogram' => $foodprogram ,
				//'exerciseprogram' => $exerciseprogram ,
				'fitness' => $fitness ,
				'balance' => $balance ,
				'payment' => $payment ,
				'date' => $date,
				'time' => $time ,
				'facebookname' => $facebookname,
				'facebooklink' => $facebooklink ,
        'shirt' => $shirt,
				//'60dcondition' => $condition ,
				'knowpff' => $knowpffnew,
        'gen' => 'รุ่นที่7',
        'price' => $price,
        'discount' => $discount,
        'digitalscale' => $digitalscale,
        'caliper' => $caliper,
        'total' => $total,


			);

		$this->db->insert('shop_paymentinform', $data);



		$data = array(
               'payment_status' => 1
            );

		$this->db->where('invid', $invid);
		$this->db->update('shop_order', $data);



		//Sendmail After complete
		$receiver = $email ;


		$text = 'สวัสดีครับ<br><br>

				อีเมลนี้เป็นอีเมลอัตโนมัติเพื่อแจ้งให้ท่านทราบว่า ทางทีมงานได้รับข้อมูลการแจ้งการโอนเงิน/ชำระผ่านบัตรเครดิต ของท่านแล้ว<br><br>

				ขั้นตอนต่อไป ทางฝ่ายบัญชีจะตรวจสอบยอดเงินที่ท่านแจ้งเข้ามา พร้อมส่งอีเมลอีกฉบับให้ท่านภายใน 48 ชม. ทำการ (จันทร์ถึงศุกร์ 13.00 น. - 18.00 น.)<br><br>

				หากทางทีมงานไม่พบข้อมูลการโอนเงิน/ชำระผ่านบัตรเครดิต ตามที่ท่านแจ้งเข้ามา จะมีการติดต่อผ่านทางโทรศัพท์กลับไปยังท่าน ภายใน 48 ชม. ทำการครับ<br><br>

				หากภายใน 48 ชม. ทำการ ท่านยังไม่ได้รับอีเมลคอนเฟิร์มการชำระเงิน กรุณาติดต่อที่ inbox fanpage หรือที่ลิ้ง fb.com/messages/planforfit/<br><br>

				เนื่องจากอีเมลนี้เป็นอีเมลอัตโนมัติ รบกวนท่านอย่าตอบกลับอีเมลนี้ครับ<br><br>

				ขอแสดงความนับถือ<br>
				PlanforFIT Team<br><br>

				บริษัท แพลนฟอร์ฟิต จำกัด<br>
				429/129 ถ.สรงประภา แขวงดอนเมือง เขตดอนเมือง กทม. 10210<br>
				มือถือ : 0938832339<br>
				เวลาทำการ จันทร์ถึงศุกร์ 13.00 น. - 18.00 น.<br>
				www.planforfit.com<br>
				www.facebook.com/planforfit' ;


		$maildata = array(
				'senderemail' => 'planforfit@gmail.com' ,
				'subject' => 'ทาง PlanforFIT ได้รับข้อมูลการแจ้งการโอนเงิน/ชำระผ่านบัตรเครดิต ของท่านแล้ว',
				'emailmessage' => $text,
				'sendtomail' => $receiver
		);

		$this->load->model("sendgrid_model");
		$this->sendgrid_model->sendmail($maildata);



		//redirect(site_url()."/order/ordercompleteinform","refresh");
		$this->load->view("completeinform");

	}

	//send data to server omise to check payment
	public function omisesubmit(){

		//$type = "sandBox";
		$type = "live" ;
		$token = $this->input->post('omise_token');
		$invid = $this->input->post('invid');


		$query = $this->db->get_where('shop_order', array('invid' => $invid),1)->result_array();

		try{

		if (count($query) > 0)
		{

			$price = $query[0]['total']."00";
			$callback = $this->payment->omise_pay($token,$price,$type);
			$rs = json_decode($callback, true);
			if($rs['authorized'])
			{
			//Send if Omise Complete
			$package = $this->sixtydaypromo->get_package_price($query[0]['package']);



			$receiver = $query[0]['email'] ;


      $text = '
      สวัสดีครับ<br><br>

      คำสั่งซื้อเสร็จสมบูรณ์  เลขที่คำสั่งซื้อของคุณคือ '.$query[0]['invid'].'<br><br>


      1.สำหรับท่านที่ชำระเงินผ่านบัตรเครดิต<br>
      สามารถลงทะเบียนได้ทันทีโดย   <a href="http://bit.ly/212aKM0">คลิกที่นี่</a> หรือ  http://bit.ly/212aKM0<br><br>


      2.สำหรับท่านที่ชำระเงินโดยการโอนเงินผ่านบัญชีธนาคาร<br>
      สามารถโอนได้ทันทีโดยโอนที่ ธนาคารไทยพานิชย์ เลขที่บัญชี   2782211628 บริษัท แพลนฟอร์ฟิต จำกัด ออมทรัพย์ สาขาเอสพละนาร์ด<br>
      และแจ้งการโอนเงิน    <a href="http://bit.ly/212aKM0">คลิกที่นี่</a> หรือ  http://bit.ly/212aKM0<br><br>


      3.การสอบถามข้อมูลเพิ่มเติม<br>
      - โทร 093-883-2339<br>
      - inbox fanpage : fb.com/messages/planforfit/<br><br>


      ขอแสดงความนับถือ<br>
      PlanforFIT Team<br>
      บริษัท แพลนฟอร์ฟิต จำกัด<br>
      429/129 ถ.สรงประภา แขวงดอนเมือง เขตดอนเมือง กทม. 10210<br>
      โทรศัพท์ : 020150609<br>
      มือถือ : 0938832339<br>
      เวลาทำการ จันทร์ถึงศุกร์ 13.00 น. - 18.00 น.<br>
      www.planforfit.com<br>
      www.facebook.com/planforfit<br><br>


			<br><br><br><br><br><br><center><small>หาคุณต้องการที่จะไม่ติดตาม กรุณาคลิก <a href="http://www.planforfit.com/Unsubscribe">Unsubscribe</a> </small></center>
			' ;


			$maildata = array(
					'senderemail' => 'planforfit@gmail.com' ,
					'subject' => 'คำสั่งซื้อ 60 days challenge เลขที่ '.$query[0]['invid'].' เสร็จสมบูรณ์',
					'emailmessage' => $text,
					'sendtomail' => $receiver
			);

			$this->load->model("sendgrid_model");
			$this->sendgrid_model->sendmail($maildata);





				$this->load->view('completeregister');
			}else{
				$this->load->view('error');
			}
		}else{
			$this->load->view('error');
		}
		}catch(Exception $e){
			echo $e ;
		}

	}

	//load view error
	public function ordererror(){
		$this->load->view('error');
	}

	//load omise credit card form
	public function omise_form(){
		$this->load->view('omise_form');
	}

	//ajax search order
	public function ordersearch(){

		$invid = $this->input->post('invid');

		$query = $this->db->get_where('shop_order', array('invid' => $invid))->result_array() ;

		if(count($query) >=1){
			echo json_encode($query);
		}else{
			echo json_encode(array(0=>array('email' => 'not found')));
		}
  }


    //ajax search by email
    public function ordersearchemail(){

      $email = $this->input->post('email');
      $this->db->order_by("id", "desc");
      $this->db->limit(1);
      $query = $this->db->get_where('shop_order', array('email' => $email))->result_array() ;

      if(count($query) >=1){
        echo json_encode($query);
      }else{
        echo json_encode(array(0=>array('email' => 'not found')));
      }
	   }

	//create new order after form submit and redirect to payment
    public function new_order() {

		$pffpackage = $_POST['pff-package'];

		if($pffpackage!=0){
		$pffalliance = $_POST['pff-alliance'];
		$pffgarminbuydate = $_POST['pff-garmin-buydate'];
		$pffalliancespureprice1 = $_POST['pff-alliance-spure-price1'];

		//****Check invit email
		$invitemail = $_POST['pff-invitemail'];

		$pffinv = $_POST['pff-inv'];
		$pffpic1 = $_POST['pff-pic1'];
		$pffpic2 = $_POST['pff-pic2'];
		$pffpic3 = $_POST['pff-pic3'];
		if (isset($_POST['pff-pic4']) && $_POST['pff-pic4']!='') {
			$pffpic1 = $_POST['pff-pic4'];
		}
		$pfffirstname = $_POST['pff-first-name'];
		$pfflastname = $_POST['pff-last-name'];
		$pffemail = $_POST['pff-email'];
		$pffaddress = $_POST['pff-address'];
		$pfftel = $_POST['pff-tel'];
		$pffpayment = $_POST['pff-payment'];
		$pffcondition = "";
		
		$pffemail2 = ($this->input->post("pff-mailc3p2-1")) 
			? $this->input->post("pff-mailc3p2-1") 
			: NULL;
		$pffemail3 = ($this->input->post("pff-mailc3p2-2")) 
			? $this->input->post("pff-mailc3p2-2") 
			: NULL;
		$refer_email1 = ($this->input->post("email-refer-1")) 
			? $this->input->post("email-refer-1") 
			: NULL;
		$refer_email2 = ($this->input->post("email-refer-2")) 
			? $this->input->post("email-refer-2") 
			: NULL;


    	$pffussd =  $this->input->post('pff-Dtac');

	    //New param
	    $pffgetresemail =  $this->input->post('pff-getresemail');
	    $pffscales=  $this->input->post('pff-scales');
	    $pffcliper =  $this->input->post('pff-cliper');
	    $pffshirt =  $this->input->post('pff-shirt');
	    $pffshirtsize =  $this->input->post('pff-shirtsize');

	    //Check shirt size
	    $shirt = "" ;
	    if($pffshirt == 1){
	      $shirt = $pffshirtsize;
	    }
	    //Check Add on
	    $addonprice = 0 ;
	    if($pffscales == 1){ $addonprice += 450 ;}
	    if($pffcliper == 1){ $addonprice += 350 ;}
	    if($pffshirt == 1){ $addonprice += 290 ;}

		//Get package price
		$package = $this->sixtydaypromo->get_package_price($pffpackage);


		//Calculate Discount
		if($pffalliance == 1){
			//Garmin discount
			$packagediscount = $this->sixtydaypromo->get_package_discount($pffalliance,$pffgarminbuydate);
			$pffcondition = $pffgarminbuydate;
		}else if($pffalliance == 2){
			//Spure discount
			$packagediscount = $this->sixtydaypromo->get_package_discount($pffalliance,$pffalliancespureprice1);
			$pffcondition = "Spure"." ".$pffalliancespureprice1;
		}else if($pffalliance == 3){
			//Duo package discount
			$packagediscount = $this->sixtydaypromo->get_package_discount($pffalliance,$pffpackage);
			$pffcondition = "Friend invit";
			$package['price'] *= 2 ;
		}else if($pffalliance == 4){
			$packagediscount = $this->sixtydaypromo->get_package_discount($pffalliance,$invitemail);
			$pffcondition = $invitemail;
		}else if($pffalliance == 5){
          	$packagediscount = $this->sixtydaypromo->get_package_discount($pffalliance,0);
          	$pffcondition = "หนังสือ how to ฟิตพิชิตหุ่นนายแบบ";
		}else if($pffalliance == 6){
          	$packagediscount = $this->sixtydaypromo->get_package_discount($pffalliance,$pffussd);
          	$pffcondition = "ลูกค้า Dtac";
		}else if($pffalliance == 7){
			//Tofusan discount
			$packagediscount = $this->sixtydaypromo->get_package_discount($pffalliance,$pffalliancespureprice1);
			$pffcondition = "Tofusan"." ".$pffalliancespureprice1;
		}else if($pffalliance == 8){
			//Tofusan discount
			$packagediscount = $this->sixtydaypromo->get_package_discount($pffalliance,$pffalliancespureprice1);
			$pffcondition = "ทำบุญหุ่นเฟิร์ม: บริจาคเงิน"." ".$pffalliancespureprice1;
		}else if($pffalliance == 9){
			//Tofusan discount
			$packagediscount = $this->sixtydaypromo->get_package_discount($pffalliance,$pffalliancespureprice1);
			$pffcondition = "ทำบุญหุ่นเฟิร์ม: บริจาคโลหิต, ร่างกาย"." ".$pffalliancespureprice1;
		}else if($pffalliance == 10){
			if ($this->input->post('email-refer-1') && $this->input->post('email-refer-2')) {
				$package['price'] = 3350;
				$packagediscount = 1116;
				$pffcondition = "ราคาพิเศษ 2234";
			} else {
				$package['price'] = 3350;
				$packagediscount = 335;
				$pffcondition = "ราคาพิเศษ 3015";
			}
		}else if($pffalliance == 11){
			//Tofusan discount
			$package['price'] = 10050 ;
			$packagediscount = 3350;
			$pffcondition = "มา 3 จ่าย 2";
		}else if($pffalliance == 12){
			//Tofusan discount
			$package['price'] = 6700 ;
			$packagediscount = 1675;
			$pffcondition = "ท่านที่ 2 ลด 50%";
			$pffemail2 = $this->input->post('pff-mail-half-price');
		}else{
			$packagediscount = 0;
		}


    //Check Email getresponse
    $mail = $pffgetresemail;
    $getresdiscount = 0 ;
    $query = $this->db->get_where('shop_email_getresponse', array('email' => $mail));
    if ($query->num_rows() > 0)
    {
      $getresdiscount = 150 ;
    }
	
	
	$newdiscount = 0 ;
    //Sum discount
    /*$newdiscount = $getresdiscount + $packagediscount;
    if($newdiscount >= 300){
      $newdiscount = 300 ;
    }*/
	

	// Grand total
	$totalprice = $package['price'] - $packagediscount + $addonprice ;


		//Create Order
		$data = array(
            'package' => $pffpackage ,
            'price' => $package['price'] ,
            'discount' => $packagediscount ,
            'pic1' => $pffpic1 ,
            'pic2' => $pffpic2 ,
            'pic3' => $pffpic3 ,
            'tel' => $pfftel ,
            'alliance' => $pffalliance ,
            'alliance_condition' => $pffcondition ,
            'total' => $totalprice ,
            'invid' => $pffinv  ,
            'firstname' => $pfffirstname  ,
            'lastname' => $pfflastname  ,
            'email' => $pffemail ,
			'email2' => $pffemail2 ,
			'email3' => $pffemail3 ,
            'address' => $pffaddress ,
            'payment' => $pffpayment,
            'payment_status' => 0 ,
            'shirt' => $shirt ,
            'digitalscale' => $pffscales,
            'caliper' => $pffcliper,
            'email_getresponse' => $pffgetresemail,
            'email_refer1' => $refer_email1,
            'email_refer2' => $refer_email2,
		);

		$query = $this->db->get_where('shop_order', array('invid' => $pffinv));
		if ($query->num_rows() > 0)
		{

		}else{
			$this->db->insert('shop_order', $data);
		}





		if($pffpayment == 2 || $pffpayment == '2'){

			//Send data to paypal and redirect
			$callback = $this->payment->paypal_pay($package['name'],$pffinv,$totalprice);
			//echo($callback);
			header('Location: '.$callback);


		}elseif($pffpayment == 3 || $pffpayment == '3'){

			//Load Omise Form
			$data['invid'] = $pffinv ;
			$this->load->view('omise_form',$data);

		}else{



      $text = '
      สวัสดีครับ<br><br>

      คำสั่งซื้อเสร็จสมบูรณ์  เลขที่คำสั่งซื้อของคุณคือ '.$pffinv.'<br><br>
      

      สำหรับท่านที่สมัครโปรโมชั่น "สมัครเป็นคู่ ท่านที่ 2 ลด 50%" และ "มา 3 จ่าย 2" สามารถแยกชำระเงินได้โดย<br>
      - โปรโมชั่น "สมัครเป็นคู่ ท่านที่ 2 ลด 50%" <a href="http://goo.gl/REuw9u">คลิกที่นี่</a> หรือ http://goo.gl/REuw9u เพื่ออ่านรายละเอียด<br>
      - โปรโมชั่น "มา 3 จ่าย 2" <a href="http://goo.gl/qZeh0k">คลิกที่นี่</a> หรือ http://goo.gl/qZeh0k เพื่ออ่านรายละเอียด<br><br>


      เมื่อชำระเงินเรียบร้อยแล้ว รบกวนแจ้งการชำระเงิน ดังนี้<br><br>


      1.สำหรับท่านที่ชำระเงินผ่านบัตรเครดิต<br>
      สามารถลงทะเบียนได้ทันทีโดย   <a href="http://bit.ly/212aKM0">คลิกที่นี่</a> หรือ  http://bit.ly/212aKM0<br><br>


      2.สำหรับท่านที่ชำระเงินโดยการโอนเงินผ่านบัญชีธนาคาร<br>
      สามารถโอนได้ทันทีโดยโอนที่ ธนาคารไทยพานิชย์ เลขที่บัญชี   2782211628 บริษัท แพลนฟอร์ฟิต จำกัด ออมทรัพย์ สาขาเอสพละนาร์ด<br>
      และแจ้งการโอนเงิน    <a href="http://bit.ly/212aKM0">คลิกที่นี่</a> หรือ  http://bit.ly/212aKM0<br><br>


      3.การสอบถามข้อมูลเพิ่มเติม<br>
      - โทร 093-883-2339<br>
      - inbox fanpage : fb.com/messages/planforfit/<br><br>


      ขอแสดงความนับถือ<br>
      PlanforFIT Team<br>
      บริษัท แพลนฟอร์ฟิต จำกัด<br>
      429/129 ถ.สรงประภา แขวงดอนเมือง เขตดอนเมือง กทม. 10210<br>
      โทรศัพท์ : 020150609<br>
      มือถือ : 0938832339<br>
      เวลาทำการ จันทร์ถึงศุกร์ 13.00 น. - 18.00 น.<br>
      www.planforfit.com<br>
      www.facebook.com/planforfit<br><br>


			<br><br><br><br><br><br><center><small>หาคุณต้องการที่จะไม่ติดตาม กรุณาคลิก <a href="http://www.planforfit.com/Unsubscribe">Unsubscribe</a> </small></center>
			' ;


			$maildata = array(
					'senderemail' => 'planforfit@gmail.com' ,
					'subject' => 'คำสั่งซื้อ 60 days challenge เลขที่ '.$pffinv.' เสร็จสมบูรณ์',
					'emailmessage' => $text,
					'sendtomail' => $pffemail
			);

			$this->load->model("sendgrid_model");
			$this->sendgrid_model->sendmail($maildata);

			if ( ! empty($pffemail2)) {
				$this->sendgrid_model->sendmail(array(
					'senderemail' => 'planforfit@gmail.com' ,
					'subject' => 'คำสั่งซื้อ 60 days challenge เลขที่ '.$pffinv.' เสร็จสมบูรณ์',
					'emailmessage' => $text,
					'sendtomail' => $pffemail2
				));
			}
			if( ! empty($pffemail3)) {
				$this->sendgrid_model->sendmail(array(
					'senderemail' => 'planforfit@gmail.com' ,
					'subject' => 'คำสั่งซื้อ 60 days challenge เลขที่ '.$pffinv.' เสร็จสมบูรณ์',
					'emailmessage' => $text,
					'sendtomail' => $pffemail3
				));
			}

			if($pffalliance == 10) {
				$this->_send_refer_mail($refer_email1, $refer_email2);
			}


			$this->load->view('completeregister');
		}


		}



    }

    private function _send_refer_mail($refer_email1, $refer_email2) {
    	$text = '
สวัสดีครับ<br><br>

Email นี้ส่งถึงคุณเพื่อแจ้งให้ทราบว่า เพื่อนของคุณได้เข้าร่วมโปรแกรม 60 Days Challenge และ "คุณ" คือ คนที่เพื่อนคัดเลือกให้มาเปลี่ยนแปลงตัวเองไปพร้อมกับเขา<br><br>

60 Days Challenge ... โปรแกรม Online Training ที่ช่วยเปลี่ยนแปลงรูปร่างผู้เข้าร่วมไปแล้วกว่า 1,500 คน<br><br>

9 ใน 10 เห็นผลจริง ด้วยวิธีการใหม่ เน้นที่การซ่อมระบบเผาผลาญก่อน กินอะไรก็ได้ ออกกำลังกายตอนไหนก็ได้ อยู่ที่ไหนก็มีหุ่นที่ดีได้ และ "คุณ" ก็มีหุ่นที่ดีได้ใน 60 วัน <br><br>

อย่าปล่อยให้เพื่อนคุณหุ่นดีคนเดียว ... คลิกที่รูป สมัครเลย!!!<br><br>
หรือ <a href="http://www.planforfit.com/60days/" target="_blank" title="ไปยัง http://www.planforfit.com/60days/">คลิกที่นี่</a> หรือ <a href="http://www.planforfit.com/60days/" target="_blank" title="ไปยัง http://www.planforfit.com/60days/">http://www.planforfit.com/60days</a> <br>
<a href="http://www.planforfit.com/60days/" target="_blank" title="ไปยัง http://www.planforfit.com/60days/"><img src="http://www.planforfit.com/60d_special/assets/imgs/refer600px.jpg" alt="สมัครได้ที่ http://www.planforfit.com/60days/"/></a>
    	' ;

		$this->load->model("sendgrid_model");
		$subject = 'เพื่อนของคุณกำลังจะหุ่นดี และได้ชวนคุณมาหุ่นดีพร้อม ๆ กัน!!!';
		$this->sendgrid_model->sendmail(array(
			'senderemail' => 'planforfit@gmail.com' ,
			'subject' => $subject,
			'emailmessage' => $text,
			'sendtomail' => $refer_email1
		));
		$this->sendgrid_model->sendmail(array(
			'senderemail' => 'planforfit@gmail.com' ,
			'subject' => $subject,
			'emailmessage' => $text,
			'sendtomail' => $refer_email2
		));
    }


    //ajax upload bill picture
    public function picupload() {

		if(isset($_FILES["file1"]))
		{
			$uploadfile = $_FILES["file1"];
		}else if(isset($_FILES["file2"])){
			$uploadfile = $_FILES["file2"];
		}
		else if(isset($_FILES["file3"])){
			$uploadfile = $_FILES["file3"];
		}else if(isset($_FILES["file4"])){
    			$uploadfile = $_FILES["file4"];
    		}
		else{
			return false ;
		}

		$fileName = $uploadfile["name"]; // The file name
		$fileTmpLoc = $uploadfile["tmp_name"]; // File in the PHP tmp folder
		$fileType = $uploadfile["type"]; // The type of file it is
		$fileSize = $uploadfile["size"]; // File size in bytes
		$fileErrorMsg = $uploadfile["error"]; // 0 for false... and 1 for true


		if (!$fileTmpLoc) { // if file not chosen
			echo "ERROR: Please browse for a file before clicking the upload button.";
			exit();
		}
		if(move_uploaded_file($fileTmpLoc, "upload/$fileName")){
			$microtime = str_replace(".","",microtime(true));
			$newname = $microtime.$fileName ;
			rename("upload/$fileName", "upload/$newname");
			echo $newname;

		} else {
			echo "move_uploaded_file function failed";
		}
    }




}
