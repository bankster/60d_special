<?php

class Account extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function decodepassword($mail) {


        $sqlstr = "SELECT usr_id,usr_login,usr_password,DECODE( usr_password, '".$this->config->item('salt')."' ) FROM Users WHERE usr_email  = '" . $mail . "'";
        $rs = $this->db->query($sqlstr)->result_array();

        
        $val["id"] = $rs[0]["usr_id"];
        $val["usr"] = $rs[0]["usr_login"];
        $val["dpws"] = $rs[0]["usr_password"];
        $val["epws"] = $rs[0]["DECODE( usr_password, '".$this->config->item('salt')."' )"];
        
        //Return 
        //--Username
        //--DecodePassword
        //--EncodePassword
        return $val;
    }

    public function updateaffmoney($affid, $money, $format) {

        $money = intval($money);

        if ($format == 'THB') {
            $money = $money / 30;
            echo $money . "เงินที่ได้ <br/>";
        }
        if ($format == 'EUR') {
            $money = $money * 1.5;
        }
        if ($format == 'USD') {
            $money = $money;
        }
        
        $money = ($money*60)/100 ;

        $sqlstr = "UPDATE Users SET usr_money= usr_money+'" . $money . "' WHERE usr_id  = '" . $affid . "'";
        $row = $this->db->query($sqlstr);
    }

    public function DateTimeDiff($strDateTime1, $strDateTime2) {
        return (strtotime($strDateTime2) - strtotime($strDateTime1)) / ( 60 * 60 ); // 1 Hour =  60*60
    }

    public function accountexist($mail) {
        $rs = $this->db->where("usr_email", $mail)->get("Users")->result_array();
        $numrow = count($rs);
        $val['exist'] = FALSE ;

        if ($numrow > 0) {
            $val['exist'] = TRUE;
        } 
        
        $val['affid'] = $rs[0]['usr_aff_id'];
        
        return $val ;
         
    }

    public function createpremium($userlogin, $pws, $useremail, $day, $aff = "0") {

        $userpassword = "ENCODE($pws,'" . $this->config->item('salt') . "')";
        $userstatus = 'OK';
        $userpremiumexpire = 'NOW() + INTERVAL ' . $day . ' DAY';
        $useraffid = $aff;
        $usercreatedate = 'NOW()';


        $sqlstr = "INSERT INTO Users ";
        $sqlstr .="(usr_login,usr_password,usr_email,usr_status,usr_premium_expire,usr_aff_id,usr_created) ";
        $sqlstr .="VALUES ";
        $sqlstr .= "('" . $userlogin . "'," . $userpassword . ",'" . $useremail . "','" . $userstatus . "'," . $userpremiumexpire . ",'" . $useraffid . "'," . $usercreatedate . ")";
        $rs = $this->db->query($sqlstr);
        $userid = $this->db->insert_id();
        return $userid;
    }

    public function updatepremium($day, $mail) {

        $rs = $this->db->where("usr_email", $mail)->get("Users")->result_array();

        $datetime1 = $rs[0]["usr_premium_expire"];
        date_default_timezone_set("Asia/Bangkok");
        $datetime2 = date('Y-m-d H:i:s');
        $interval = $this->DateTimeDiff($datetime2, $datetime1);

        if ($interval < 0) {
            //หมดอายุ
            $userpremiumexpire = 'NOW() + INTERVAL ' . $day . ' DAY';
        } else {
            //ยังไม่หมดอายุ
            $date = date_create($datetime1);
            date_add($date, date_interval_create_from_date_string("$day days"));

            $userpremiumexpire = date_format($date, 'Y-m-d H:i:s');
            $userpremiumexpire = "'" . $userpremiumexpire . "'";
        }


        $sqlstr = "UPDATE Users SET usr_premium_expire=" . $userpremiumexpire . " WHERE usr_email  = '" . $mail . "'";
        $this->db->query($sqlstr);
    }


}
