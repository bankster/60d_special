<?php

class Payment extends CI_Model {


    //Call Omise API
	public function omise_pay($token,$price,$type){

			//$Parameter = "package=".$package['name'] ."&invid=".$pffinv."&total=".$totalprice;
			$Parameter = "price=".$price."&token=".$token."&type=".$type;
			$API_URL = 'http://planforfit.com/payment/omise/pay.php';

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $API_URL);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $Parameter);

			$Result = curl_exec($ch);
			curl_close($ch);
			return $Result;
	}
	
	
	//Call Paypal API
	public function paypal_pay($package,$pffinv,$totalprice){
	
			//Redirect to paypal
			$Parameter = "package=".$package."&invid=".$pffinv."&total=".$totalprice;
			$API_URL = 'http://planforfit.com/payment/paypal/vendor/paypal/rest-api-sdk-php/sample/pp.php';

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $API_URL);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $Parameter);

			$Result = curl_exec($ch);
			curl_close($ch);
			return $Result;

	
	}

   

}

?>



