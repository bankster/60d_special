<?php

class Sixtydaypromo extends CI_Model {


	public function get_package_price($package){

		switch ($package) {
		case 1:
			$rs['price'] = 3290 ;
			$rs['name'] = "Starter package ราคา 3290";
			break;
		case 2:
			$rs['price'] = 4290 ;
			$rs['name'] = "Standard package ราคา 4290";
			break;
		case 3:
			$rs['price'] = 9450 ;
			$rs['name'] = "Premium package ราคา 9450";
			break;
		default:
			$rs['price'] = 0 ;
			$rs['name'] ="";
		}

		return $rs;

	}

	public function get_package_discount($pffalliance,$pffcondition)
	{
		$discount = 0 ;
		switch ($pffalliance) {

		//Garmin Case
		case 1:
			if($pffcondition == 1){$discount = 250;}
			elseif ($pffcondition == 2){$discount = 500;}
			else{$discount = 0;};
			break;

		//S-Pure Case
		case 2:
			// 50% discount not over 500
			// $discount = $pffcondition/2 ;
			// if($discount >= 500){$discount = 500;}
			//New Promotion
			$discount = $pffcondition ;
			if($discount >= 300){$discount = 300;}
			break;

		//Duo package
		case 3:
			if($pffcondition ==1){$discount = 600 ;}
			elseif($pffcondition ==2){$discount = 800 ;}
			elseif($pffcondition ==3){$discount = 1000 ;}
			else{$pffcondition = 0;}
			break;

		//Friend Invit
		case 4:
			//Check Email Promotion
			$query = $this->db->get_where('shop_email_promo', array('email' => $pffcondition));
			if ($query->num_rows() > 0)
			{
				$discount = 300 ;
			}else{
				$discount =  0 ;
			}
			break;

		//Modeling Book
		case 5:
			$discount = 300 ;
			break;

		//Ussd Code
		case 6:
				$query = $this->db->get_where('shop_ussd_promo', array('ussd_code' => $pffcondition))->result_array();
				if (count($query) > 0)
				{
					$discount = $query[0]['discount'] ;
				}else{
					$discount =  0 ;
				}
				break;
				
				
	    //S-Pure Case
		case 7:
			// 50% discount not over 500
			// $discount = $pffcondition/2 ;
			// if($discount >= 500){$discount = 500;}
			//New Promotion
			$discount = $pffcondition ;
			if($discount >= 300){$discount = 300;}
			break;
			
		//ทำบุญบริจาคเงิน
		case 8:
			// 50% discount not over 500
			// $discount = $pffcondition/2 ;
			// if($discount >= 500){$discount = 500;}
			//New Promotion
			$discount = $pffcondition ;
			if($discount >= 300){$discount = 300;}
			break;

			
			
			
		//ทำบุญบริจากเลือด + ร่างกาย
		case 9:
			// 50% discount not over 500
			// $discount = $pffcondition/2 ;
			// if($discount >= 500){$discount = 500;}
			//New Promotion
			$discount = $pffcondition ;
			if($discount >= 300){$discount = 300;}
			break;


		default:
				$discount = 0 ;
				break;

		}// End Switch

		return $discount;

	}


}

?>
