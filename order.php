<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Order extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        // Call the Model constructor

        parent::__construct();
		$this->load->model('memail');
		$this->load->model('sixtydaypromo');
		$this->load->model('payment');
    }

    public function index() {
		
    }
	
	
	public function mailcheck(){
		$mail = $_POST['email'];
		$query = $this->db->get_where('shop_order_promo', array('email' => $mail));
		if ($query->num_rows() > 0)
		{
			echo 1 ;
		}
	}
	
	
	//load view ordercomplete
	public function ordercomplete()
	{
		$this->load->view('complete');
	}
	
	//load view payment inform
	public function paymentinform()
	{
		$this->load->view('paymentinform');
	}
	
	//Insert paymentinform to database
	public function paymentinformsubmit()
	{
		
			$invid = $_POST['invid'];
			$join60d_reson = $_POST['join60d_reson'];
			$name = 	$_POST['name'];
			$sex =	$_POST['sex'];
			$tel =	$_POST['tel'];
			$age =	$_POST['age'];
			$email = $_POST['email'];
			$height = $_POST['height'];
			$weight = 	$_POST['weight'];
			$address =	$_POST['address'];
			$fatpercent =	$_POST['fatpercent'];
			$target =	$_POST['target'];
			$disease = $_POST['disease'];
			$foodprogram = $_POST['foodprogram'];
			$exerciseprogram = 	$_POST['exerciseprogram'];
			$fitness =	$_POST['fitness'];
			$balance =	$_POST['balance'];
			$payment =	$_POST['payment'];
			$date =	$_POST['date'];
			$time =	$_POST['time'];
			$condition =	$_POST['60dcondition'];
			$knowpff =	$_POST['knowpff'];
			$knowpffnew = "";
			
			foreach($knowpff as $val)
			{
				$knowpffnew .= $val."," ; 
			}

			
			$data = array(
				'invid' => $invid ,
				'join60d_reson' => $join60d_reson ,
				'name' => $name ,
				'sex' => $sex ,
				'tel' => $tel ,
				'age' => $age ,
				'email' => $email ,
				'height' => $height ,
				'weight' => $weight ,
				'address' => $address ,
				'fatpercent' => $fatpercent ,
				'target' => $target ,
				'disease' => $disease ,
				'foodprogram' => $foodprogram ,
				'exerciseprogram' => $exerciseprogram ,
				'fitness' => $fitness ,
				'balance' => $balance ,
				'payment' => $payment ,
				'time' => $time ,
				'60dcondition' => $condition ,
				'knowpff' => $knowpffnew 
			);
			
		$this->db->insert('shop_paymentinform', $data); 
		
		
		
		$data = array(
               'payment_status' => 1
            );

		$this->db->where('invid', $invid);
		$this->db->update('shop_order', $data); 
		
		//redirect(site_url()."/order/ordercompleteinform","refresh");
		$this->load->view("completeinform");
				
	}
	
	//send data to server omise to check payment 
	public function omisesubmit(){
		
		//$type = "sandBox";
		$type = "live" ;
		$token = $this->input->post('omise_token');
		$invid = $this->input->post('invid');
		
		
		$query = $this->db->get_where('shop_order', array('invid' => $invid),1)->result_array();
		
		try{
		
		if (count($query) > 0)
		{
			
			$price = $query[0]['total']."00";
			$callback = $this->payment->omise_pay($token,$price,$type);
			$rs = json_decode($callback, true);
			if($rs['authorized'])
			{
				$this->load->view('completeregister');
			}else{
				$this->load->view('error');
			}
		}else{
			$this->load->view('error');
		}
		}catch(Exception $e){
			echo $e ;
		}
		
	}
	
	//load view error
	public function ordererror()
	{
		$this->load->view('error');
	}
	
	
	//load omise credit card form
	public function omise_form(){
		$this->load->view('omise_form');
	}
	
	//ajax search order
	public function ordersearch(){
		
		$invid = $_POST['invid'];
		
		$query = $this->db->get_where('shop_order', array('invid' => $invid))->result_array() ;
		echo json_encode($query);
		 

	}
	
	//create new order after form submit and redirect to payment
    public function new_order() {
		
		$pffpackage = $_POST['pff-package'];
		
		if($pffpackage!=0){
		$pffalliance = $_POST['pff-alliance'];
		$pffgarminbuydate = $_POST['pff-garmin-buydate'];
		$pffalliancespureprice1 = $_POST['pff-alliance-spure-price1'];
		
		//****Check invit email
		$invitemail = $_POST['pff-invitemail'];
		
		$pffinv = $_POST['pff-inv'];
		$pffpic1 = $_POST['pff-pic1'];
		$pffpic2 = $_POST['pff-pic2'];
		$pffpic3 = $_POST['pff-pic3'];
		$pfffirstname = $_POST['pff-first-name'];
		$pfflastname = $_POST['pff-last-name'];
		$pffemail = $_POST['pff-email'];
		$pffaddress = $_POST['pff-address'];
		$pfftel = $_POST['pff-tel'];
		$pffpayment = $_POST['pff-payment'];
		$pffcondition = "";
		
		//Get package price
		$package = $this->sixtydaypromo->get_package_price($pffpackage);
		
		
		//Calculate Discount
		if($pffalliance == 1){
					//Garmin discount
					$packagediscount = $this->sixtydaypromo->get_package_discount($pffalliance,$pffgarminbuydate);
					$pffcondition = $pffgarminbuydate;
		}else if($pffalliance == 2){
					//Spure discount
					$packagediscount = $this->sixtydaypromo->get_package_discount($pffalliance,$pffalliancespureprice1);
					$pffcondition = $pffalliancespureprice1;				
		}else if($pffalliance == 3){
					//Duo package discount
					$packagediscount = $this->sixtydaypromo->get_package_discount($pffalliance,$pffpackage);
					$pffcondition = "Friend invit";	
					$package['price'] *= 2 ;					
		}else if($pffalliance == 4){
					$packagediscount = $this->sixtydaypromo->get_package_discount($pffalliance,$invitemail);
					$pffcondition = $invitemail;				
		}else{
					$packagediscount = 0;
		}
		
		// Grand total
		$totalprice = $package['price'] - $packagediscount ;
		
		
		//Create Order 
		$data = array(
		   'package' => $pffpackage ,
		   'price' => $package['price'] ,
		   'discount' => $packagediscount ,
			'pic1' => $pffpic1 ,
			'pic2' => $pffpic2 ,
			'pic3' => $pffpic3 ,
			'tel' => $pfftel ,
			'alliance' => $pffalliance ,
			'alliance_condition' => $pffcondition ,
		   'total' => $totalprice ,
		   'invid' => $pffinv  ,
		   'firstname' => $pfffirstname  ,
		   'lastname' => $pfflastname  ,
		   'email' => $pffemail ,
		   'address' => $pffaddress ,
		   'payment' => $pffpayment,
		   'payment_status' => 0
		);
		
		$query = $this->db->get_where('shop_order', array('invid' => $pffinv));
		if ($query->num_rows() > 0)
		{
			
		}else{
			$this->db->insert('shop_order', $data); 
		}
		
		
	
		
		
		if($pffpayment == 2 || $pffpayment == '2'){
			
			//Send data to paypal and redirect
			$callback = $this->payment->paypal_pay($package['name'],$pffinv,$totalprice);
			//echo($callback);
			header('Location: '.$callback);

			
		}elseif($pffpayment == 3 || $pffpayment == '3'){
			
			//Load Omise Form
			$data['invid'] = $pffinv ;
			$this->load->view('omise_form',$data);
			
		}else{
			
			//Send mail to choice Bank tranfer
			$text = 'รายละเอียดการสั่งซื้อ <br>
			เลขที่รายการ  :  '.$pffinv.'<br>
			รายการ  :  '.$package['name'].'<br>
			ราคา :  '.$package['price'].'<br>
			ส่วนลด  :  '.$packagediscount.'<br>
			ราคารวม  :  '.$totalprice.'<br>
			ท่านสามารชำระเงินได้ที่ธนาคารไทยพานิชย์ เลขที่บัญชี 2782211628 บริษัท แพลนฟอร์ฟิต จำกัด ออมทรัพย์ สาขาเอสพลานาด<br>
			
			
			<br><br><br><br><br><br><center><small>หาคุณต้องการที่จะไม่ติดตาม กรุณาคลิก <a href="http://www.planforfit.com/Unsubscribe">Unsubscribe</a> </small></center>
			' ;
			$admintext = "aa" ;
			$receiver = $pffemail ;	
			$this->memail->sendsem($text, $admintext, $receiver);
			
			//echo site_url()."/order/ordercomplete" ;
			//redirect(site_url()."/order/ordercomplete","refresh");
			$this->load->view('completeregister');
		}
		
		
		}
		
		

    }
	
	
    //ajax upload bill picture
    public function picupload() {
		
		if(isset($_FILES["file1"]))
		{
			$uploadfile = $_FILES["file1"];
		}else if(isset($_FILES["file2"])){
			$uploadfile = $_FILES["file2"];
		}
		else if(isset($_FILES["file3"])){
			$uploadfile = $_FILES["file3"];
		}
		else{
			return false ;
		}

		$fileName = $uploadfile["name"]; // The file name
		$fileTmpLoc = $uploadfile["tmp_name"]; // File in the PHP tmp folder
		$fileType = $uploadfile["type"]; // The type of file it is
		$fileSize = $uploadfile["size"]; // File size in bytes
		$fileErrorMsg = $uploadfile["error"]; // 0 for false... and 1 for true
		
		
		if (!$fileTmpLoc) { // if file not chosen
			echo "ERROR: Please browse for a file before clicking the upload button.";
			exit();
		}
		if(move_uploaded_file($fileTmpLoc, "upload/$fileName")){
			$microtime = str_replace(".","",microtime(true));
			$newname = $microtime.$fileName ; 
			rename("upload/$fileName", "upload/$newname");
			echo $newname;
			
		} else {
			echo "move_uploaded_file function failed";
		}
    }
	

}

