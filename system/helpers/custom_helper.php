<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('getuserinfo')) {

    function getuserinfo($magentoid = '') {
        
        
        $CI = & get_instance();
        $result = $CI->db->where('us_autoid',$magentoid)->get('tb_usermember')->result_array();
        $data = array(
            'name' => $result[0]['us_name'] ,
            'mail' => $result[0]['us_mail'] ,
            'sex' => $result[0]['us_sex'] ,
            'tel' => $result[0]['us_tel']
        );

        return  $data;
        //return $CI->config->base_url($uri);
        
    }

}
?>