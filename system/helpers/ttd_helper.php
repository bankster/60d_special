<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 *
 * @package		TTD
 * @author		Touchtechdesign
 * @copyright	        Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */
// ------------------------------------------------------------------------

/**
 * CodeIgniter Array Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/helpers/array_helper.html
 */
// ------------------------------------------------------------------------

/**
 * Element
 *
 * Lets you determine whether an array index is set and whether it has a value.
 * If the element is empty it returns FALSE (or whatever you specify as the default value.)
 *
 * @access	public
 * @param	string
 * @param	array
 * @param	mixed
 * @return	mixed	depends on what the array contains
 */
if (!function_exists('getcatname')) {

    function getcardserial($cardid) {

        $ci = & get_instance();
        $a = $ci->db->where('cds_id', $cardid)->join('cardtype', 'cardtype.cdt_id = cardstock.cds_typeid')->get('cardstock')->result_array();
        //echo $a[0]['cds_serial'];
        return  $a;

    }


}

// ------------------------------------------------------------------------


/* End of file gadoor_helper.php */
/* Location: ./system/helpers/gadoor_helper.php */