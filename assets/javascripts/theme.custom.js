/* Add here all your JS customizations */
var windowsHeight = $(window).height();
	$('#paymentinfor , #completeRegister , #completeinform').height(windowsHeight);

if( $(window).width() < 767 ) {
	$('#paymentinfor .tab-content').css('max-height', windowsHeight - 190);
	var paymentinfor_contentbox  = $('#paymentinfor .tab-content').height();
	$('#paymentinfor .col-lg-12').css('height', paymentinfor_contentbox + 190);


	$('#completeRegister .contentbox').css('max-height', windowsHeight - 230);
	var completeRegister_contentbox  = $('#completeRegister .contentbox').height();
	$('#completeRegister .col-lg-12').css('height', completeRegister_contentbox + 180);

	$('#completeinform .contentbox').css('max-height', windowsHeight - 230);
	var completeinform_contentbox  = $('#completeinform .contentbox').height();
	$('#completeinform .col-lg-12').css('height', completeinform_contentbox + 180);

} else {
	$('#paymentinfor .tab-content').css('max-height', windowsHeight - 300);
	var paymentinfor_contentbox  = $('#paymentinfor .tab-content').height();
	$('#paymentinfor .col-lg-12').css('height', paymentinfor_contentbox + 541);

	$('#completeRegister .contentbox').css('max-height', windowsHeight - 230);
	var completeRegister_contentbox  = $('#completeRegister .contentbox').height();
	$('#completeRegister .col-lg-12').css('height', completeRegister_contentbox + 180);

}




$(window).resize(function(event) {
	var windowsHeightRe = $(window).height();
	$('#paymentinfor , #completeRegister').height(windowsHeightRe);

	var userAgent = navigator.userAgent.toLowerCase();
	var isAndroid = userAgent.indexOf("android") > -1; //&& ua.indexOf("mobile");
	if(isAndroid) {
		//alert('android');
	} else {
		if( $(window).width() < 767 ) {
			$('#paymentinfor , #completeRegister').height(windowsHeightRe);

			$('#paymentinfor .tab-content').css('max-height', windowsHeightRe - 190);
			var paymentinfor_contentbox_re  = $('#paymentinfor .tab-content').height();
			$('#paymentinfor .col-lg-12').css('height', paymentinfor_contentbox_re + 190);

			$('#completeRegister .contentbox').css('max-height', windowsHeightRe - 230);
			var completeRegister_contentbox_re  = $('#completeRegister .contentbox').height();
			$('#completeRegister .col-lg-12').css('height', completeRegister_contentbox_re + 180);

			$('#completeinform .contentbox').css('max-height', windowsHeightRe - 230);
			var completeinform_contentbox_re  = $('#completeinform .contentbox').height();
			$('#completeinform .col-lg-12').css('height', completeinform_contentbox_re + 180);
		} else {
			$('#paymentinfor .tab-content').css('max-height', windowsHeightRe - 300);
			var paymentinfor_contentbox_re  = $('#paymentinfor .tab-content').height();
			$('#paymentinfor .col-lg-12').css('height', paymentinfor_contentbox_re + 541);

			$('#completeRegister .contentbox').css('max-height', windowsHeightRe - 230);
			var completeRegister_contentbox_re  = $('#completeRegister .contentbox').height();
			$('#completeRegister .col-lg-12').css('height', completeRegister_contentbox_re + 180);
		}
	}
});